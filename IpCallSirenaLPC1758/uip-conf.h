#ifndef __UIP_CONF_H__
#define __UIP_CONF_H__

#include "common_type.h"
#include "uip/uip_type.h"

// Size of ARP table
#define UIP_CONF_ARPTAB_SIZE        8

// uIP buffer size
#define UIP_CONF_BUFFER_SIZE        1536

// uIP statistics on or off
#define UIP_CONF_STATISTICS         0

// Broadcast Support
#define UIP_CONF_BROADCAST          0

// Link-Level Header length
#define UIP_CONF_LLH_LEN            14

// CPU byte order.
#define UIP_CONF_BYTE_ORDER         UIP_LITLE_ENDIAN

#endif // __UIP_CONF_H_
