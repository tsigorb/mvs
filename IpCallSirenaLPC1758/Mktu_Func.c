#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <LPC17xx.h>      // Keil: Register definition file for LPC17xx
#include "easyudp.h"

//////////////////////////////////////////////////////////////////////////
void LedGreenToogle(void)
{
  static bool bLedGreenOn=false;

  LPC_GPIO0->FIODIR |=d_LED_GREEN;
  if(bLedGreenOn)
  { LPC_GPIO0->FIOSET=d_LED_GREEN; bLedGreenOn=false; }
  else
  { LPC_GPIO0->FIOCLR=d_LED_GREEN; bLedGreenOn=true; }
}

void SetUIP_IpMaskMAC(InitEthUdpTp *psInitData)
{
  uip_ipaddr_t ipaddr;

  uip_init();   // Initialize the uIP TCP/IP stack.
  uip_ipaddr(ipaddr,
             ((psInitData->dwIp>>24) & 0xff),
             ((psInitData->dwIp>>16) & 0xff),
             ((psInitData->dwIp>>8) & 0xff),
             (psInitData->dwIp & 0xff));
  uip_sethostaddr(ipaddr);
  uip_ipaddr(ipaddr,
             ((psInitData->dwMask>>24) & 0xff),
             ((psInitData->dwMask>>16) & 0xff),
             ((psInitData->dwMask>>8) & 0xff),
             (psInitData->dwMask & 0xff));
  uip_setnetmask(ipaddr);
  uip_setport((u16_t)(psInitData->wPort));
  uip_setethaddr(psInitData->btMAC_Addr);
}

//////////////////////////////////////////////////////////////////////////
#define d_SETTINGS2FLASHSECTOR  18  // SEE UM10360!
#define d_SETTINGS2FLASHADDR    0x20000
#define d_SETTINGSBUFSZ         (sizeof(InitEthUdpTp)+((sizeof(g_IpTelNumbs)>>2)<<2)+4)

typedef void (*IAP)(DWORD *cmd,DWORD *res);
IAP IAP_entry=(IAP)0x1FFF1FF1;

bool FlashErase(void)
{
  DWORD dwMasCmd[5]={50,d_SETTINGS2FLASHSECTOR,d_SETTINGS2FLASHSECTOR},
        dwMasResult[5];

  IAP_entry(dwMasCmd,dwMasResult);
  if(dwMasResult[0]!=CMD_SUCCESS) return(false); // Error prepare sector(s) for write operation

  dwMasCmd[0]=52;
  dwMasCmd[3]=SystemFrequency/1000;
  IAP_entry(dwMasCmd,dwMasResult);
  if(dwMasResult[0]!=CMD_SUCCESS) return(false); // Error erase sector(s)
  return(true);
}

bool FlashProgram(unsigned long *pdwBuf,unsigned long dwSz0)
{
  DWORD dwMasCmd[5]={50,d_SETTINGS2FLASHSECTOR,d_SETTINGS2FLASHSECTOR},
        dwMasResult[5];

  IAP_entry(dwMasCmd,dwMasResult);
  if(dwMasResult[0]!=CMD_SUCCESS) return(false); // Error prepare sector(s) for write operation

  dwMasCmd[0]=51;
  dwMasCmd[1]=d_SETTINGS2FLASHADDR;
  dwMasCmd[2]=(DWORD)pdwBuf;
  dwMasCmd[3]=dwSz0;
  dwMasCmd[4]=SystemFrequency/1000;
  IAP_entry(dwMasCmd,dwMasResult);
  if(dwMasResult[0]!=CMD_SUCCESS) return(false); // Error write data to flash
  return(true);
}

DWORD CalcCRC4Flash(PBYTE pbtData,WORD wSz)
{
  DWORD dwCRC=0;

  while(wSz)
  {
    wSz--;
    dwCRC+=pbtData[wSz];
  }
  return(dwCRC);
}

int StoreSettings2Flash(InitEthUdpTp *psInitData)
{
  DWORD dwCRC;
  PBYTE btSetBuf;
  DWORD dwSetBuf[d_SETTINGSBUFSZ >> 2];

  btSetBuf=(PBYTE)dwSetBuf;
  memcpy(btSetBuf,(void *)psInitData->btMAC_Addr,6);
  memcpy(btSetBuf+6,(void *)&psInitData->dwIp,4);
  memcpy(btSetBuf+10,(void *)&psInitData->dwMask,4);
  memcpy(btSetBuf+14,(void *)&psInitData->wPort,2);
  memcpy(btSetBuf+16,g_IpTelNumbs,sizeof(g_IpTelNumbs));
  dwCRC=CalcCRC4Flash(btSetBuf,d_SETTINGSBUFSZ-4);
  memcpy(btSetBuf+d_SETTINGSBUFSZ-4,&dwCRC,4);
  if(!memcmp((PBYTE)d_SETTINGS2FLASHADDR,btSetBuf,d_SETTINGSBUFSZ)) return(d_MVSSET_EQ);
  if(FlashErase()) FlashProgram(dwSetBuf,256);
  if(memcmp((PBYTE)d_SETTINGS2FLASHADDR,btSetBuf,d_SETTINGSBUFSZ)) return(d_MVSSET_ERRWR);
  return(d_MVSSET_OK);
}

int ReadSettingsFromFlash(InitEthUdpTp *psInitData)
{
  DWORD dwCRC;
  PBYTE pbtAddr=(PBYTE)d_SETTINGS2FLASHADDR;

  memcpy(&dwCRC,pbtAddr+d_SETTINGSBUFSZ-4,4);
  if(CalcCRC4Flash(pbtAddr,d_SETTINGSBUFSZ-4)==dwCRC)
  {
    memcpy((void *)&psInitData->btMAC_Addr,pbtAddr,6);
    memcpy((void *)&psInitData->dwIp,pbtAddr+6,4);
    memcpy(g_IpTelNumbs,pbtAddr+16,sizeof(g_IpTelNumbs));
    return(0);
  }
  return(-1);
}

void *memfind(void *pData,int iDataLen,void *pExample,int iExampleLen)
{
  PBYTE pbtData=(PBYTE)pData;

  do
  {
    if(iExampleLen<=iDataLen)
    {
      if(!memcmp(pbtData,pExample,iExampleLen)) return(pbtData);
      pbtData++;
      iDataLen--;
    }
  }
  while(iExampleLen<=iDataLen);
  return(NULL);
}

int GetInfoElement(PCHAR pchBuf,PCHAR strInfo,int iNumbEl)
{
  int iCurEl,iLn;
  PCHAR pchStart,pchFinish;

  pchBuf[0]=0;
  pchStart=(PCHAR)strInfo;
  for(iCurEl=0;iCurEl<=iNumbEl;iCurEl++)
  {
    pchStart=strchr(pchStart,'=');
    if(!pchStart) return(0);
    pchStart++;
  }
  pchFinish=strchr(pchStart,',');
  if(!pchFinish) pchFinish=strchr(pchStart,'.');
  if(!pchFinish) return(0);
  if((pchStart+1)==pchFinish) return(0);
  iLn=pchFinish-pchStart;
  if(iLn>19) return(0);
  memcpy(pchBuf,pchStart,iLn);
  pchBuf[iLn]=0;
  return(iLn);
}

BYTE Simb2Code(char chSimb)
{
  if((chSimb>='0') && (chSimb<='9')) return (BYTE)(chSimb-'0');
  if((chSimb>='a') && (chSimb<='f')) return (BYTE)(chSimb-'a'+10);
  if((chSimb>='A') && (chSimb<='F')) return (BYTE)(chSimb-'A'+10);
  return(0xff);
}

DWORD ElementCalcCRC(PCHAR pchElement)
{
  int i,iLn=strlen(pchElement);
  DWORD dwCRC=0;

  for(i=0;i<iLn;i++) dwCRC+=~Simb2Code(pchElement[i]);
  return(dwCRC);
}

DWORD CalcCRC(PCHAR strInfo)
{
  DWORD dwCRC=0;
  static char chBuf[20];

  if(GetInfoElement(chBuf,strInfo,2)) dwCRC+=ElementCalcCRC(chBuf);
  dwCRC&=0xffff;
  if(GetInfoElement(chBuf,strInfo,3)) dwCRC+=ElementCalcCRC(chBuf);
  dwCRC&=0xffff;
  if(GetInfoElement(chBuf,strInfo,4)) dwCRC+=ElementCalcCRC(chBuf);
  dwCRC&=0xffff;
  if(GetInfoElement(chBuf,strInfo,0)) dwCRC|=ElementCalcCRC(chBuf) << 16;
  return(dwCRC);
}

DWORD STR08HEX2DW(PCHAR pchData)
{
  int i;
  BYTE btCode;
  DWORD dwRes=0;

  if((pchData[0]!='0') || (pchData[1]!='x')) return(0);
  pchData+=2;
  for(i=0;i<8;i++)
  {
    btCode=Simb2Code(pchData[i]);
    if(btCode==0xff) return(0);
    dwRes=(dwRes<<4) | btCode;
  }
  return(dwRes);
}

void ConvertStr2MAC(PBYTE pbtMAC0,PCHAR pStr)
{
  int i;
  BYTE btCode,btRes;
  PBYTE pbtMAC=pbtMAC0;

  for(i=0;i<6;i++,pbtMAC++)
  {
    btCode=Simb2Code(*pStr);
    if(btCode==0xff) break;
    btRes=btCode<<4;
    pStr++;
    btCode=Simb2Code(*pStr);
    if(btCode==0xff) break;
    btRes|=btCode;
    pStr++;
    *pbtMAC=btRes;
  }
  if(i!=6) memcpy(pbtMAC0,g_btMAC_Default,6);
}

int MVSSettingsPacket(InitEthUdpTp *psInitData,PCHAR pchIpData,WORD wSize0)
{
  DWORD dwCRC;
  PCHAR strMVSSet="MVS settings: IPADDR=";
  static char chBuf[20];

  if(memfind(pchIpData,wSize0,strMVSSet,strlen(strMVSSet))==(void *)pchIpData)
  {
    pchIpData[wSize0]=0;
    if(GetInfoElement(chBuf,pchIpData,6)==10)
    {
      dwCRC=STR08HEX2DW(chBuf);
      if(dwCRC==CalcCRC(pchIpData))
      {
        GetInfoElement(chBuf,pchIpData,0);
        psInitData->dwIp=STR08HEX2DW(chBuf);
        GetInfoElement(chBuf,pchIpData,1);
        if(strlen(chBuf)==12) ConvertStr2MAC(psInitData->btMAC_Addr,chBuf);
        GetInfoElement(chBuf,pchIpData,2);
        strcpy(g_IpTelNumbs[0],chBuf);
        GetInfoElement(chBuf,pchIpData,3);
        strcpy(g_IpTelNumbs[1],chBuf);
        GetInfoElement(chBuf,pchIpData,4);
        strcpy(g_IpTelNumbs[2],chBuf);
        GetInfoElement(chBuf,pchIpData,5); // MAC for answer from MVS!
        if(strlen(chBuf)==12) ConvertStr2MAC(g_btSrsMAC,chBuf);
        return(StoreSettings2Flash(psInitData));
      }
    }
    return(d_MVSSET_ERRFMT);
  }
  return(d_MVSSET_NOCMD);
}
