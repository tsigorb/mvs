#ifndef LPC_1768

#include "emac.h"
#include <LPC17xx.h>                    /* LPC17xx definitions               */

#define MDIO    0x00000008   // P0.3
#define MDC     0x00000004   // P0.2

typedef unsigned int U32;

//////////////////////////////////////////////////////////////////////////
static void delay(void)
{
  int i;
  for(i=0;i<10;i++);
}

static void output_MDIO (U32 val, U32 n)
{ // Output a value to the MII PHY management interface
  for (val <<= (32 - n); n; val <<= 1, n--)
  {
    if (val & 0x80000000) LPC_GPIO0->FIOSET = MDIO;
    else LPC_GPIO0->FIOCLR = MDIO;
    delay();
    LPC_GPIO0->FIOSET = MDC;
    delay();
    LPC_GPIO0->FIOCLR = MDC;
  }
}

static void turnaround_MDIO(void)
{ // Turnaround MDO is tristated
   LPC_GPIO0->FIODIR &= ~MDIO;
   LPC_GPIO0->FIOSET  = MDC;
   delay ();
   LPC_GPIO0->FIOCLR  = MDC;
   delay ();
}

static U32 input_MDIO(void)
{ // Input a value from the MII PHY management interface
  U32 i,val = 0;

  for (i = 0; i < 16; i++)
  {
    val <<= 1;
    LPC_GPIO0->FIOSET = MDC;
    delay ();
    LPC_GPIO0->FIOCLR = MDC;
    if (LPC_GPIO0->FIOPIN & MDIO) val |= 1;
  }
  return (val);
}

U32 mdio_read(int PhyReg)
{
   U32 val;

   /* Configuring MDC on P0.2 and MDIO on P0.3 */
   LPC_GPIO0->FIODIR |= MDIO | MDC;

   /* 32 consecutive ones on MDO to establish sync */
   output_MDIO (0xFFFFFFFF, 32);

   /* start code (01), read command (10) */
   output_MDIO (0x06, 4);

   /* write PHY address */
   output_MDIO (DP83848C_DEF_ADR >> 8, 5);

   /* write the PHY register to write */
   output_MDIO (PhyReg, 5);

   /* turnaround MDO is tristated */
   turnaround_MDIO ();

   /* read the data value */
   val = input_MDIO ();

   /* turnaround MDIO is tristated */
   turnaround_MDIO ();

   return (val);
}

void mdio_write(int PhyReg, int Value)
{
  /* Configuring MDC on P0.2 and MDIO on P0.3 */
  LPC_GPIO0->FIODIR |= MDIO | MDC;

  /* 32 consecutive ones on MDO to establish sync */
  output_MDIO (0xFFFFFFFF, 32);

  /* start code (01), write command (01) */
  output_MDIO (0x05, 4);

  /* write PHY address */
  output_MDIO (DP83848C_DEF_ADR >> 8, 5);

  /* write the PHY register to write */
  output_MDIO (PhyReg, 5);

  /* turnaround MDIO (1,0)*/
  output_MDIO (0x02, 2);
  
  /* write the data value */
  output_MDIO (Value, 16);

  /* turnaround MDO is tristated */
  turnaround_MDIO ();
}

#endif
