#ifndef __UIP_H__
#define __UIP_H__

#include "uipopt.h"

typedef u16_t uip_ip4addr_t[2];
typedef uip_ip4addr_t uip_ipaddr_t;

extern uip_ipaddr_t uip_hostaddr, uip_netmask, uip_draddr;
extern u16_t uip_wPort;

#define uip_sethostaddr(addr) uip_ipaddr_copy(uip_hostaddr, (addr))
#define uip_gethostaddr(addr) uip_ipaddr_copy((addr), uip_hostaddr)

#define uip_setdraddr(addr) uip_ipaddr_copy(uip_draddr, (addr))
#define uip_getdraddr(addr) uip_ipaddr_copy((addr), uip_draddr)

#define uip_setnetmask(addr) uip_ipaddr_copy(uip_netmask, (addr))
#define uip_getnetmask(addr) uip_ipaddr_copy((addr), uip_netmask)

void uip_setport(u16_t wPort);

#define uip_newdata()   (g_uip_flags & UIP_NEWDATA)

#define uip_ipaddr(addr, addr0,addr1,addr2,addr3) do { \
                     ((u16_t *)(addr))[0] = HTONS(((addr0) << 8) | (addr1)); \
                     ((u16_t *)(addr))[1] = HTONS(((addr2) << 8) | (addr3)); \
                  } while(0)

#define uip_ipaddr_copy(dest, src) do { \
                     ((u16_t *)dest)[0] = ((u16_t *)src)[0]; \
                     ((u16_t *)dest)[1] = ((u16_t *)src)[1]; \
                  } while(0)

#define uip_ipaddr_cmp(addr1, addr2) (((u16_t *)addr1)[0]==((u16_t *)addr2)[0] && \
                                      ((u16_t *)addr1)[1]==((u16_t *)addr2)[1])

#define uip_ipaddr_maskcmp(addr1, addr2, mask) \
                          (((((u16_t *)addr1)[0] & ((u16_t *)mask)[0]) == \
                            (((u16_t *)addr2)[0] & ((u16_t *)mask)[0])) && \
                           ((((u16_t *)addr1)[1] & ((u16_t *)mask)[1]) == \
                            (((u16_t *)addr2)[1] & ((u16_t *)mask)[1])))

#define uip_ipaddr_mask(dest, src, mask) do { \
                     ((u16_t *)dest)[0] = ((u16_t *)src)[0] & ((u16_t *)mask)[0]; \
                     ((u16_t *)dest)[1] = ((u16_t *)src)[1] & ((u16_t *)mask)[1]; \
                  } while(0)

#define uip_ipaddr1(addr) (htons(((u16_t *)(addr))[0]) >> 8)

#define uip_ipaddr2(addr) (htons(((u16_t *)(addr))[0]) & 0xff)

#define uip_ipaddr3(addr) (htons(((u16_t *)(addr))[1]) >> 8)

#define uip_ipaddr4(addr) (htons(((u16_t *)(addr))[1]) & 0xff)

#ifndef HTONS
#   if UIP_BYTE_ORDER == UIP_BIG_ENDIAN
#      define HTONS(n) (n)
#   else /* UIP_BYTE_ORDER == UIP_BIG_ENDIAN */
#      define HTONS(n) (u16_t)((((u16_t) (n)) << 8) | (((u16_t) (n)) >> 8))
#   endif /* UIP_BYTE_ORDER == UIP_BIG_ENDIAN */
#else
#error "HTONS already defined!"
#endif /* HTONS */

// The structure holding the TCP/IP statistics that are gathered if UIP_STATISTICS is set to 1
struct uip_stats 
{
  struct {
    uip_stats_t drop;     /**< Number of dropped packets at the IP layer. */
    uip_stats_t recv;     /**< Number of received packets at the IP layer. */
    uip_stats_t sent;     /**< Number of sent packets at the IP layer. */
    uip_stats_t vhlerr;   /**< Number of packets dropped due to wrong IP version or header length. */
    uip_stats_t hblenerr; /**< Number of packets dropped due to wrong IP length, high byte. */
    uip_stats_t lblenerr; /**< Number of packets dropped due to wrong IP length, low byte. */
    uip_stats_t fragerr;  /**< Number of packets dropped since they were IP fragments. */
    uip_stats_t chkerr;   /**< Number of packets dropped due to IP checksum errors. */
    uip_stats_t protoerr; /**< Number of packets dropped since they were neither ICMP, UDP nor TCP. */
  } ip;                   /**< IP statistics. */

  struct {
    uip_stats_t drop;     /**< Number of dropped ICMP packets. */
    uip_stats_t recv;     /**< Number of received ICMP packets. */
    uip_stats_t sent;     /**< Number of sent ICMP packets. */
    uip_stats_t typeerr;  /**< Number of ICMP packets with a wrong type. */
  } icmp;                 /**< ICMP statistics. */

  struct {
    uip_stats_t drop;     /**< Number of dropped UDP segments. */
    uip_stats_t recv;     /**< Number of recived UDP segments. */
    uip_stats_t sent;     /**< Number of sent UDP segments. */
    uip_stats_t chkerr;   /**< Number of UDP segments with a bad checksum. */
  } udp;                  /**< UDP statistics. */
};

extern struct uip_stats uip_stat;

extern u16_t g_uip_flags;

/* The following flags may be set in the global variable uip_flags
   before calling the application callback. The UIP_ACKDATA,
   UIP_NEWDATA, and UIP_CLOSE flags may both be set at the same time,
   whereas the others are mutualy exclusive. Note that these flags
   should *NOT* be accessed directly, but only through the uIP functions/macros. */
#define UIP_ACKDATA   0x01    /* Signifies that the outstanding data was acked and the application should send
                                 out new data instead of retransmitting the last data. */
#define UIP_NEWDATA   0x02    /* Flags the fact that the peer has sent us new data. */
#define UIP_REXMIT    0x04    /* Tells the application to retransmit the data that was last sent. */
#define UIP_POLL      0x08    /* Used for polling the application, to check if the application has data that it wants to send. */
#define UIP_CLOSE     0x10    /* The remote host has closed the connection, thus the connection has gone away.
                                 Or the application signals that it wants to close the connection. */
#define UIP_ABORT     0x20    /* The remote host has aborted the connection, thus the connection has gone away.
                               Or the application signals that it wants to abort the connection. */
#define UIP_CONNECTED 0x40    /* We have got a connection from a remote host and have set up a new connection for it,
                                 or an active connection has been successfully established. */
#define UIP_TIMEDOUT  0x80    /* The connection has been aborted due to too many retransmissions. */

#define UIP_SEND      0x100

#define g_pUipAppData_SIZE      (UIP_BUFSIZE - UIP_LLH_LEN - UIP_TCPIP_HLEN)

#define UIP_PROTO_ICMP          1
#define UIP_PROTO_UDP           17

/* Header sizes. */
#define UIP_IPH_LEN             20    /* Size of IP header */
#define UIP_UDPH_LEN            8    /* Size of UDP header */
#define UIP_IPUDPH_LEN          (UIP_UDPH_LEN + UIP_IPH_LEN)    /* Size of IP + UDP header */

///////////////////////////////////////////////////////////////////////////////
void uip_setethaddr(u8_t *addr);
void uip_init(void);
u8_t PrepareSendUdpWithMAC(u32_t dwDstIP,u16_t wDstPort,u8_t *pbtDstMAC,u16_t wDataSz);
void uip_udp_rcv(void);

void udp_data_rcv(void);

#endif /* __UIP_H__ */
