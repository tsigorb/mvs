/** uIP is a small implementation of the IP, UDP and TCP protocols (as
 * well as some basic ICMP stuff). The implementation couples the IP,
 * UDP, TCP and the application layers very tightly. To keep the size
 * of the compiled code down, this code frequently uses the goto
 * statement. While it would be possible to break the uip_process()
 * function into many smaller functions, this would increase the code
 * size because of the overhead of parameter passing and the fact that
 * the optimier would not be as efficient.
 *
 * The principle is that we have a small buffer, called the g_btUIPBufPtr,
 * in which the device driver puts an incoming packet. The TCP/IP
 * stack parses the headers in the packet, and calls the
 * application. If the remote host has sent data to the application,
 * this data is present in the g_btUIPBufPtr and the application read the
 * data from there. It is up to the application to put this data into
 * a byte stream if needed. The application will not be fed with data
 * that is out of sequence.
 *
 * If the application whishes to send data to the peer, it should put its data into the g_btUIPBufPtr.
 * The g_pUipAppData pointer points to the first available byte. The TCP/IP stack will calculate the checksums,
 * and fill in the necessary header fields and finally send the packet back to the peer. */

#include "uip.h"
#include "uipopt.h"
#include "uip_arp.h"

#if UIP_CONF_IPV6
#include "uip-neighbor.h"
#endif // UIP_CONF_IPV6

#include <string.h>

///////////////////////////////////////////////////////////////////////////////
u32_t clock_time(void);

extern u8_t g_btSrsIP[4];

///////////////////////////////////////////////////////////////////////////////
// Variable definitions
uip_ipaddr_t uip_hostaddr, uip_draddr, uip_netmask;
u16_t uip_wPort;

struct uip_eth_addr uip_ethaddr={{0,0,0,0,0,0}};

void *g_pUipAppData; // The g_pUipAppData pointer points to application data.

u16_t g_wUipLen;

u16_t g_uip_flags=0;

struct uip_udpip_hdr *g_psUdpHdr;

// Temporary variables.
static u16_t tmp16;

///////////////////////////////////////////////////////////////////////////////
#define ICMP_ECHO_REPLY 0
#define ICMP_ECHO       8

// Macros
#define FBUF        ((struct uip_tcpip_hdr *)&uip_reassbuf[0])
//#define ICMPBUF     ((struct uip_icmpip_hdr *)&g_btUIPBufPtr[UIP_LLH_LEN])
#define IPHDR       ((struct ethip_hdr *)&g_btUIPBufPtr[0])

#if UIP_STATISTICS==1
struct uip_stats uip_stat;
#define UIP_STAT(s) s
#else
#define UIP_STAT(s)
#endif // UIP_STATISTICS==1

//---------------------------------------------------------------------------
static u16_t chksum(u16_t sum, const u8_t *data, u16_t len)
{
  u16_t t;
  const u8_t *dataptr;
  const u8_t *last_byte;

  dataptr=data;
  last_byte=data + len - 1;
  while(dataptr < last_byte)
  { // At least two more bytes
    t=(dataptr[0] << 8) + dataptr[1];
    sum += t;
    if(sum < t) sum++; // carry
    dataptr += 2;
  }
  if(dataptr==last_byte)
  {
    t=(dataptr[0] << 8) + 0;
    sum += t;
    if(sum < t) sum++;  // carry
  }
  return sum;
}

//---------------------------------------------------------------------------
u16_t uip_chksum(u16_t *data, u16_t len)
{ return HTONS(chksum(0, (u8_t *)data, len)); }

//---------------------------------------------------------------------------
u16_t uip_ipchksum(void)
{
  u16_t sum;

  sum=chksum(0,g_btUIPBufPtr+UIP_LLH_LEN,UIP_IPH_LEN);
  return (sum==0) ? 0xffff : HTONS(sum);
}

static u16_t upper_layer_chksum(u8_t proto)
{
  u16_t upper_layer_len;
  u16_t sum;

  upper_layer_len=(((u16_t)(g_psUdpHdr->len[0]) << 8)+g_psUdpHdr->len[1])-UIP_IPH_LEN;

  // First sum pseudoheader
  // IP protocol and length fields. This addition cannot carry
  sum=upper_layer_len+proto;

  // Sum IP source and destination addresses
  sum=chksum(sum,(u8_t *)&g_psUdpHdr->srcipaddr[0],2*sizeof(uip_ipaddr_t));

  // Sum TCP header and data
  sum=chksum(sum,&g_btUIPBufPtr[UIP_IPH_LEN+UIP_LLH_LEN],upper_layer_len);
  return (sum==0) ? 0xffff : HTONS(sum);
}

u16_t uip_udpchksum(void)
{ return upper_layer_chksum(UIP_PROTO_UDP); }

void uip_setport(u16_t wPort)
{ uip_wPort=wPort; }

void uip_setethaddr(u8_t *addr)
{
  uip_ethaddr.addr[0] = addr[0];
  uip_ethaddr.addr[1] = addr[1];
  uip_ethaddr.addr[2] = addr[2];
  uip_ethaddr.addr[3] = addr[3];
  uip_ethaddr.addr[4] = addr[4];
  uip_ethaddr.addr[5] = addr[5];
}

void uip_init(void)
{
  g_psUdpHdr=(struct uip_udpip_hdr *)&g_btUIPBufPtr[UIP_LLH_LEN];
  //uip_arp_init();
}

u8_t PrepareSendUdpWithMAC(u32_t dwDstIP,u16_t wDstPort,u8_t *pbtDstMAC,u16_t wDataSz)
{
  static u16_t ipid=0;

  if(!ipid) ipid=(u16_t)clock_time();
  g_uip_flags=0;
  if(wDataSz==0) return(0);
  g_wUipLen=UIP_IPUDPH_LEN+wDataSz;
  g_uip_flags=UIP_SEND;

  memcpy(IPHDR->ethhdr.dest.addr,pbtDstMAC,6);
  memcpy(IPHDR->ethhdr.src.addr,uip_ethaddr.addr,6);
//{
//  u8_t btMAC_HP[6]={0xfc,0x15,0xb4,0x04,0x80,0x1c};
//  memcpy(IPHDR->ethhdr.src.addr,btMAC_HP,6);
//}
  IPHDR->ethhdr.type=HTONS(UIP_ETHTYPE_IP);

  g_psUdpHdr->len[0]=(g_wUipLen >> 8);
  g_psUdpHdr->len[1]=(g_wUipLen & 0xff);
  g_psUdpHdr->ttl=UIP_TTL;
  g_psUdpHdr->proto=UIP_PROTO_UDP;
  g_psUdpHdr->udplen=HTONS(g_wUipLen-UIP_IPUDPH_LEN+UIP_UDPH_LEN);
  g_psUdpHdr->destport=HTONS(wDstPort);
  g_psUdpHdr->destipaddr[0]=dwDstIP & 0xffff;
  g_psUdpHdr->destipaddr[1]=dwDstIP >> 16;
  g_psUdpHdr->srcport=HTONS(uip_wPort);
  uip_ipaddr_copy(g_psUdpHdr->srcipaddr, uip_hostaddr);
//g_psUdpHdr->srcport=HTONS(54180);
//g_psUdpHdr->srcipaddr[1]=0xFA01;
  g_psUdpHdr->udpchksum=0;    // NO DELETE THIS LINE!!!
#if UIP_UDP_CHECKSUMS
  g_psUdpHdr->udpchksum=~(uip_udpchksum());
  if(g_psUdpHdr->udpchksum==0) g_psUdpHdr->udpchksum=0xffff;
#else
  g_psUdpHdr->udpchksum=0;
#endif
//g_psUdpHdr->udpchksum=0x5be8;
  g_psUdpHdr->vhl=0x45;
  g_psUdpHdr->tos=0;
  g_psUdpHdr->ipoffset[0]=g_psUdpHdr->ipoffset[1]=0;
  ++ipid;
//ipid=3053;
  g_psUdpHdr->ipid[0]=ipid >> 8;
  g_psUdpHdr->ipid[1]=ipid & 0xff;

  // Calculate IP checksum
  g_psUdpHdr->ipchksum=0;   // NO DELETE THIS LINE!!!
  g_psUdpHdr->ipchksum=~(uip_ipchksum());
//g_psUdpHdr->ipchksum=0x54aa;

  g_wUipLen+=UIP_LLH_LEN;

  UIP_STAT(++uip_stat.udp.sent);
  UIP_STAT(++uip_stat.ip.sent);
  return(wDataSz);
}

void uip_udp_rcv(void)
{
  u16_t wUipLen_Save;

  g_pUipAppData=&g_btUIPBufPtr[UIP_IPUDPH_LEN+UIP_LLH_LEN];
  // This is where the input processing starts.
  UIP_STAT(++uip_stat.ip.recv);
  // Start of IP input header processing code.
  // Check validity of the IP header.
  if(g_psUdpHdr->vhl!=0x45)
  { // IP version and header length.
    UIP_STAT(++uip_stat.ip.drop);
    UIP_STAT(++uip_stat.ip.vhlerr);
    //UIP_LOG("ip: invalid version or header length.");
    goto drop;
  }
  /* Check the size of the packet. If the size reported to us in
     g_wUipLen is smaller the size reported in the IP header, we assume
     that the packet has been corrupted in transit. If the size of
     g_wUipLen is larger than the size reported in the IP packet header,
     the packet has been padded and we set g_wUipLen to the correct value. */
  wUipLen_Save=g_wUipLen;
  tmp16=(u16_t)g_psUdpHdr->len[0];
  tmp16=(tmp16 << 8)+g_psUdpHdr->len[1];
  if(tmp16<=g_wUipLen) g_wUipLen=tmp16;
  else
  { goto drop; } // ip packet shorter than reported in IP header
  // Check the fragment flag.
  if(((g_psUdpHdr->ipoffset[0] & 0x3f)!=0) || (g_psUdpHdr->ipoffset[1]!=0))
  {
#if UIP_REASSEMBLY
    g_wUipLen=uip_reass();
    if(g_wUipLen==0) goto drop;
#else // UIP_REASSEMBLY
    UIP_STAT(++uip_stat.ip.drop);
    UIP_STAT(++uip_stat.ip.fragerr);
    //UIP_LOG("ip: fragment dropped.");
    goto drop;
#endif // UIP_REASSEMBLY
  }
  {
    /*if((g_psUdpHdr->proto==UIP_PROTO_UDP) && (uip_ipaddr_cmp(g_psUdpHdr->destipaddr,all_ones_addr)))
    { // udp broadcast packet recieved
      goto udp_input;
    }*/
    // Check if the packet is destined for our IP address.
    if(!uip_ipaddr_cmp(g_psUdpHdr->destipaddr, uip_hostaddr))
    {
      if(!uip_ipaddr_cmp(g_psUdpHdr->srcipaddr, uip_hostaddr))
      {
        UIP_STAT(++uip_stat.ip.drop);
        goto drop;
      }
    }
  }
  if(uip_ipchksum()!=0xffff)
  { // Compute and check the IP header checksum.
    UIP_STAT(++uip_stat.ip.drop);
    UIP_STAT(++uip_stat.ip.chkerr);
    //UIP_LOG("ip: bad checksum.");
    goto drop;
  }
  if(g_psUdpHdr->proto==UIP_PROTO_UDP) goto udp_input;
  // ICMPv4 processing code follows
  if(g_psUdpHdr->proto!=UIP_PROTO_ICMP)
  { // We only allow ICMP packets from here.
    UIP_STAT(++uip_stat.ip.drop);
    UIP_STAT(++uip_stat.ip.protoerr);
    //UIP_LOG("ip: neither tcp nor icmp.");
    goto drop;
  }
  UIP_STAT(++uip_stat.icmp.recv);
{ /* ICMP echo (i.e., ping) processing. This is simple, we only change the ICMP type from ECHO
     to ECHO_REPLY and adjust the ICMP checksum before we return the packet. */
  struct uip_icmpip_hdr *ICMPBUF=((struct uip_icmpip_hdr *)&g_btUIPBufPtr[UIP_LLH_LEN]);

  if(ICMPBUF->type!=ICMP_ECHO)
  {
    UIP_STAT(++uip_stat.icmp.drop);
    UIP_STAT(++uip_stat.icmp.typeerr);
    //UIP_LOG("icmp: not icmp echo.");
    goto drop;
  }
  /* If we are configured to use ping IP address assignment, we use
     the destination IP address of this ping packet and assign it to ourself. */
  ICMPBUF->type=ICMP_ECHO_REPLY;
  if(ICMPBUF->icmpchksum >= HTONS(0xffff - (ICMP_ECHO << 8)))
  { ICMPBUF->icmpchksum += HTONS(ICMP_ECHO << 8) + 1; }
  else
  { ICMPBUF->icmpchksum += HTONS(ICMP_ECHO << 8); }
  // Swap MAC addresses
  memcpy(IPHDR->ethhdr.dest.addr,IPHDR->ethhdr.src.addr,6);
  memcpy(IPHDR->ethhdr.src.addr,uip_ethaddr.addr,6);
  // Swap IP addresses
  uip_ipaddr_copy(g_psUdpHdr->destipaddr,g_psUdpHdr->srcipaddr);
  uip_ipaddr_copy(g_psUdpHdr->srcipaddr,uip_hostaddr);

  g_wUipLen=wUipLen_Save;
  g_uip_flags=0;

  UIP_STAT(++uip_stat.icmp.sent);
  UIP_STAT(++uip_stat.ip.sent);
  return;  // End of IPv4 input header processing code.
}

// UDP input processing
udp_input:
  g_wUipLen=g_wUipLen-UIP_IPUDPH_LEN;
  /* UDP processing is really just a hack. We don't do anything to the UDP/IP headers,
     but let the UDP application do all the hard work */
/*
  #if UIP_UDP_CHECKSUMS
  if((g_psUdpHdr->udpchksum!=0) && (uip_udpchksum()!=0xffff))
  {
    UIP_STAT(++uip_stat.udp.drop);
    UIP_STAT(++uip_stat.udp.chkerr);
    //UIP_LOG("udp: bad checksum.");
    goto drop;
  }
#else // UIP_UDP_CHECKSUMS
*/
  uip_ipaddr_copy(g_btSrsIP,g_psUdpHdr->srcipaddr);
//#endif // UIP_UDP_CHECKSUMS
  udp_data_rcv();
  if(g_uip_flags==UIP_SEND)
  { g_uip_flags=0; return; }
drop:
  g_wUipLen=0;
  g_uip_flags=0;
}

//---------------------------------------------------------------------------
// XXX: IP fragment reassembly: not well-tested.
#if UIP_REASSEMBLY && !UIP_CONF_IPV6

#define UIP_REASS_BUFSIZE           (UIP_BUFSIZE-UIP_LLH_LEN)
#define UIP_REASS_FLAG_LASTFRAG     0x01
#define IP_MF                       0x20

static u8_t uip_reassbuf[UIP_REASS_BUFSIZE];

static u8_t uip_reassbitmap[UIP_REASS_BUFSIZE/(8*8)];

static const u8_t bitmap_bits[8]={0xff, 0x7f, 0x3f, 0x1f, 0x0f, 0x07, 0x03, 0x01};

static u16_t uip_reasslen;

static u8_t uip_reassflags;

static u8_t uip_reasstmr;

static u8_t uip_reass(void)
{
  u16_t offset, len;
  u16_t i;

  /* If ip_reasstmr is zero, no packet is present in the buffer, so we
     write the IP header of the fragment into the reassembly buffer.
     The timer is updated with the maximum age. */
  if(uip_reasstmr==0)
  {
    memcpy(uip_reassbuf, &g_psUdpHdr->vhl, UIP_IPH_LEN);
    uip_reasstmr=UIP_REASS_MAXAGE;
    uip_reassflags=0;
    // Clear the bitmap
    memset(uip_reassbitmap, 0, sizeof(uip_reassbitmap));
  }
  /* Check if the incoming fragment matches the one currently present
     in the reasembly buffer. If so, we proceed with copying the fragment into the buffer. */
  if(g_psUdpHdr->srcipaddr[0]==FBUF->srcipaddr[0] &&
     g_psUdpHdr->srcipaddr[1]==FBUF->srcipaddr[1] &&
     g_psUdpHdr->destipaddr[0]==FBUF->destipaddr[0] &&
     g_psUdpHdr->destipaddr[1]==FBUF->destipaddr[1] &&
     g_psUdpHdr->ipid[0]==FBUF->ipid[0] &&
     g_psUdpHdr->ipid[1]==FBUF->ipid[1])
  {
    len=(g_psUdpHdr->len[0]<<8)+g_psUdpHdr->len[1]-(g_psUdpHdr->vhl & 0x0f)*4;
    offset=(((g_psUdpHdr->ipoffset[0] & 0x3f)<<8)+g_psUdpHdr->ipoffset[1])*8;
    // If the offset or the offset + fragment length overflows the reassembly buffer, we discard the entire packet.
    if((offset>UIP_REASS_BUFSIZE) || ((offset+len)>UIP_REASS_BUFSIZE))
    { uip_reasstmr=0; goto nullreturn; }

    // Copy the fragment into the reassembly buffer, at the right offset.
    memcpy(&uip_reassbuf[UIP_IPH_LEN+offset],(char *)BUF+(int)((g_psUdpHdr->vhl & 0x0f)*4),len);
    // Update the bitmap.
    if((offset/(8*8))==((offset+len)/(8*8)))
    { // If the two endpoints are in the same byte, we only update that byte.
      uip_reassbitmap[offset/(8*8)]|=bitmap_bits[(offset/8) & 7] & (~bitmap_bits[((offset+len)/8) & 7]);
    }
    else
    {
      /* If the two endpoints are in different bytes, we update the
         bytes in the endpoints and fill the stuff inbetween with 0xff. */
      uip_reassbitmap[offset/(8*8)]|=bitmap_bits[(offset/8)&7];
      for(i=1+offset/(8*8);i<(offset+len)/(8*8);++i) uip_reassbitmap[i]=0xff;
      uip_reassbitmap[(offset+len)/(8*8)]|=~bitmap_bits[((offset+len)/8)&7];
    }
    /* If this fragment has the More Fragments flag set to zero, we know that this is the last fragment,
       so we can calculate the size of the entire packet.
       We also set the IP_REASS_FLAG_LASTFRAG flag to indicate that we have received the final fragment. */
    if((g_psUdpHdr->ipoffset[0] & IP_MF)==0)
    {
      uip_reassflags |= UIP_REASS_FLAG_LASTFRAG;
      uip_reasslen=offset+len;
    }
    /* Finally, we check if we have a full packet in the buffer.
       We do this by checking if we have the last fragment and if all bits in the bitmap are set. */
    if(uip_reassflags & UIP_REASS_FLAG_LASTFRAG)
    {
      // Check all bytes up to and including all but the last byte in the bitmap.
      for(i=0;i<uip_reasslen/(8*8)-1;++i)
      {
        if(uip_reassbitmap[i]!=0xff) goto nullreturn;
      }
      // Check the last byte in the bitmap. It should contain just the right amount of bits.
      if(uip_reassbitmap[uip_reasslen/(8*8)]!=(u8_t)~bitmap_bits[(uip_reasslen/8) & 7]) goto nullreturn;
      /* If we have come this far, we have a full packet in the buffer,
         so we allocate a pbuf and copy the packet into it. We also reset the timer. */
      uip_reasstmr=0;
      memcpy(BUF,FBUF,uip_reasslen);
      // Pretend to be a "normal" (i.e., not fragmented) IP packet from now on.
      g_psUdpHdr->ipoffset[0]=g_psUdpHdr->ipoffset[1]=0;
      g_psUdpHdr->len[0]=uip_reasslen >> 8;
      g_psUdpHdr->len[1]=uip_reasslen & 0xff;
      g_psUdpHdr->ipchksum=~(uip_ipchksum());
      return uip_reasslen;
    }
  }
 nullreturn:
  return 0;
}
#endif // UIP_REASSEMBLY
