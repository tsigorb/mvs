#ifndef __UIP_TYPE_H__
#define __UIP_TYPE_H__

typedef unsigned char u8_t;
typedef unsigned short u16_t;
typedef unsigned long u32_t;

// Statistics datatype
// This typedef defines the dataype used for keeping statistics in uIP.
typedef unsigned short uip_stats_t;

#pragma push
#pragma pack(1)

// Representation of a 48-bit Ethernet address.
struct uip_eth_addr { u8_t addr[6]; };

// The Ethernet header.
struct uip_eth_hdr
{
  struct uip_eth_addr dest;
  struct uip_eth_addr src;
  u16_t type;
};

#define UIP_LLH_LEN     sizeof(struct uip_eth_hdr)

struct ethip_hdr
{
  struct uip_eth_hdr ethhdr;
  // IP header
  u8_t vhl,tos,len[2],ipid[2],ipoffset[2],ttl,proto;
  u16_t ipchksum;
  u16_t srcipaddr[2],destipaddr[2];
};

/* The UDP and IP headers. */
struct uip_udpip_hdr
{
  /* IP header. */
  u8_t vhl,
    tos,
    len[2],
    ipid[2],
    ipoffset[2],
    ttl,
    proto;
  u16_t ipchksum;
  u16_t srcipaddr[2],
    destipaddr[2];
  
  /* UDP header. */
  u16_t srcport,destport;
  u16_t udplen;
  u16_t udpchksum;
};

/* The ICMP and IP headers. */
struct uip_icmpip_hdr
{
  /* IPv4 header. */
  u8_t vhl,
    tos,
    len[2],
    ipid[2],
    ipoffset[2],
    ttl,
    proto;
  u16_t ipchksum;
  u16_t srcipaddr[2],
    destipaddr[2];
  
  /* ICMP (echo) header. */
  u8_t type, icode;
  u16_t icmpchksum;
  u16_t id, seqno;
};

struct arp_hdr
{
  struct uip_eth_hdr ethhdr;
  u16_t hwtype;
  u16_t protocol;
  u8_t hwlen;
  u8_t protolen;
  u16_t opcode;
  struct uip_eth_addr shwaddr;
  u16_t sipaddr[2];
  struct uip_eth_addr dhwaddr;
  u16_t dipaddr[2];
};

struct arp_entry
{
  u16_t ipaddr[2];
  struct uip_eth_addr ethaddr;
  u8_t time;
};

#pragma pop

// ���������� ����������, ������������ � � ���������� uIP-��������� � � ����������
/* ����������� �� ������� ����� ����� ������ ��� ������ �� Ethernet.
   ����������� � ����� ����������. ������������ uIp-�������� */
extern u8_t *g_btUIPBufPtr;

/* The length of the packet in the g_btUIPBufPtr buffer.
 * The global variable g_wUipLen holds the length of the packet in the g_btUIPBufPtr buffer.
 *
 * When the network device driver calls the uIP input function,
 * g_wUipLen should be set to the length of the packet in the g_btUIPBufPtr buffer.
 * When sending packets, the device driver should use the contents of the
 * g_wUipLen variable to determine the length of the outgoing packet. */
extern u16_t g_wUipLen;

#endif
