#ifndef __UIP_ARP_H__
#define __UIP_ARP_H__

#include "uip.h"

extern struct uip_eth_addr uip_ethaddr;

#define UIP_ETHTYPE_ARP 0x0806
#define UIP_ETHTYPE_IP  0x0800

/* The uip_arp_arpin() should be called when an ARP packet is received
   by the Ethernet driver. This function also assumes that the
   Ethernet frame is present in the uip_buf buffer. When the
   uip_arp_arpin() function returns, the contents of the uip_buf
   buffer should be sent out on the Ethernet if the g_wUipLen variable is > 0 */
void uip_arp_arpin(void);

/* The uip_arp_out() function should be called when an IP packet
   should be sent out on the Ethernet. This function creates an
   Ethernet header before the IP header in the uip_buf buffer. The
   Ethernet header will have the correct Ethernet MAC destination
   address filled in if an ARP table entry for the destination IP
   address (or the IP address of the default router) is present. If no
   such table entry is found, the IP packet is overwritten with an ARP
   request and we rely on TCP to retransmit the packet that was
   overwritten. In any case, the g_wUipLen variable holds the length of
   the Ethernet frame that should be transmitted. */
//void uip_arp_out(void);

/* The uip_arp_timer() function should be called every ten seconds.
   It is responsible for flushing old entries in the ARP table. */
//void uip_arp_timer(void);

#endif /* __UIP_ARP_H__ */
