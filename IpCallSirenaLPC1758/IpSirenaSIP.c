#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <LPC17xx.h>      // Keil: Register definition file for LPC17xx
#include "easyudp.h"

//*****************************************************************************
typedef struct
{
  unsigned short wSeq;
  int iCnt;
} RTP_SEQ_TP;

//*****************************************************************************
//volatile int g_iEthInitTOut=d_EthInitTOut;

volatile int g_iSirenaTOut=0,g_iRTP_TOut=0;

volatile u8_t g_bReverceInput=0;

char g_IpTelNumbs[d_MaxIpTel][20]=
{
"!!!!",
"!!!!",
"!!!!"
};

RTP_SEQ_TP g_sRTPSeq={0,0};

char s_MasCancelSip[d_MaxIpTel][32];

///////////////////////////////////////////////////////////////////////////////
void InitCancelSIP(void)
{
  int i;
  char *strSipCmdCancel[1]=
{
"CANCEL sip:"
};

  for(i=0;i<d_MaxIpTel;i++)
  {
    sprintf(s_MasCancelSip[i],"%s%s",strSipCmdCancel[0],g_IpTelNumbs[i]);
  }
  // !!! ������������� ��� ����-��...
}

void IpCallSirena(int iNewState)
{
  if((g_iCurState==dPassive_SirenaOff) && (iNewState==dRinging_SirenaOn))
  { // �������� ������
    g_iSirenaTOut=dSirenaOnTOut;
    g_iRTP_TOut=0;
    g_iCurState=dRinging_SirenaOn;
  }
  else
  {
    if(iNewState!=dRinging_SirenaOn)
    { // ��������� ������
      if(iNewState==dPassive_SirenaOffByTOut) g_iSirenaTOut=dSirenaOffTOut;
      else g_iSirenaTOut=0;
      g_sRTPSeq.iCnt=0;
      g_iCurState=iNewState;
    }
  }
  if(g_iCurState==dRinging_SirenaOn) LPC_GPIO2->FIOSET=d_SIRENA_PIN;
  else LPC_GPIO2->FIOCLR=d_SIRENA_PIN;
}

/*
   1. ����� �������� 6150 (��������� ������)
� �������� ������ ������ ���� ��������� ������������������:
1.INVITE sip:6150@
2."To: <sip:6150@"
��� ����� �������� �������� ��� ���������������� ����������.

   2. ����� �������� 6150
recieve RTP-packets

   3. ����� ������
� �������� ������ ������ ���� ��������� ������������������:
1."CANCEL sip:6150"
*/

bool IsCallIpTlfn(void *pvNetBuf,WORD wNetSz)
{
  int i;
  char chBuf[32];

  for(i=0;i<d_MaxIpTel;i++)
  {
    if(g_bReverceInput)
    { strcpy(chBuf,"INVITE"); }
    else
    { sprintf(chBuf,"INVITE sip:%s@",g_IpTelNumbs[i]); }
    if(memfind(pvNetBuf,wNetSz,chBuf,strlen(chBuf)))
    {
      sprintf(chBuf,"To: <sip:%s@",g_IpTelNumbs[i]);
      if(memfind(pvNetBuf,wNetSz,chBuf,strlen(chBuf)))
      { return(true); }
    }
  }
  return(false);
}

bool IsAnswerIpTlfn(PBYTE pbtNetBuf,WORD wNetSz)
{
  // !!!CHECK RTP-header format!!!
  if((pbtNetBuf[0] & 0xc0)==0x80)
  {
    WORD wSeq;

    wSeq=pbtNetBuf[2];
    wSeq=(wSeq << 8) | pbtNetBuf[3];
    if(g_sRTPSeq.iCnt)
    {
      g_sRTPSeq.wSeq++;
      if(g_sRTPSeq.wSeq==wSeq)
      {
        if(g_sRTPSeq.iCnt==3)
        { g_iRTP_TOut=5; return(true); }
        g_sRTPSeq.iCnt++;
      }
      else g_sRTPSeq.iCnt=0;
    }
    else
    {
      g_sRTPSeq.iCnt=1;
      g_sRTPSeq.wSeq=wSeq;
    }
  }
  return(false);
}

bool IsOtboiIpTlfn(void *pvNetBuf,WORD wNetSz)
{
  int i;
  char chBuf[32],
       *strReqTerm="Request Terminated",
       *strBusyHere="Busy Here",
       *strACK="ACK sip:";

//  if(g_bReverceInput)
  {
    if(memfind(pvNetBuf,wNetSz,strReqTerm,strlen(strReqTerm)) ||
       memfind(pvNetBuf,wNetSz,strBusyHere,strlen(strBusyHere)))
    {
      for(i=0;i<d_MaxIpTel;i++)
      {
        sprintf(chBuf,"To: <sip:%s@",g_IpTelNumbs[i]);
        if(memfind(pvNetBuf,wNetSz,chBuf,strlen(chBuf)))
        { return(true); }
      }
    }
    else
    {
    }
  }
//  else
  {
    for(i=0;i<d_MaxIpTel;i++)
    {
      if(memfind(pvNetBuf,wNetSz,s_MasCancelSip[i],strlen(s_MasCancelSip[i]))) return(true);
    }
  }
  if(memfind(pvNetBuf,wNetSz,strACK,strlen(strACK)))
  {
    for(i=0;i<d_MaxIpTel;i++)
    {
      if(memfind(pvNetBuf,wNetSz,g_IpTelNumbs[i],strlen(g_IpTelNumbs[i]))) return(true);
    }
  }
  return(false);
}

void IpSirenaTimerWork(void)
{
  // Sirena TOut
  if(g_iSirenaTOut)
  {
    g_iSirenaTOut--;
    if(!g_iSirenaTOut)
    {
      if(g_iCurState==dPassive_SirenaOffByTOut) IpCallSirena(dPassive_SirenaOff);
      else IpCallSirena(dPassive_SirenaOffByTOut);
    }
  }

  // RTP TOut
  if(g_iRTP_TOut)
  {
    g_iRTP_TOut--;
    if(!g_iRTP_TOut) IpCallSirena(dPassive_SirenaOff);
  }
}
