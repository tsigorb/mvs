#include "EMAC.h"
#include "LPC17xx.h"

///////////////////////////////////////////////////////////////////////////////
static unsigned short *rptr;
static unsigned short *tptr;

static bool s_bInit_EMAC=false,
            s_bLinkOn=false;

///////////////////////////////////////////////////////////////////////////////
// Keil: function added to write PHY
void write_PHY(int PhyReg, int Value)
{
#ifdef LPC_1768
!!!
  unsigned int tout;

  LPC_EMAC->MADR=DP83848C_DEF_ADR | PhyReg;
  LPC_EMAC->MWTD=Value;

  // Wait utill operation completed
  tout=0;
  for(tout=0; tout < MII_WR_TOUT; tout++) {
    if((LPC_EMAC->MIND & MIND_BUSY)==0) {
      break;
    }
  }
#else
  // SOFTWARE MDIO CONTROL(SEE AN10859.pdf)
  mdio_write(PhyReg,Value);
#endif
}

// Keil: function added to read PHY
unsigned short read_PHY(unsigned char PhyReg) 
{
#ifdef LPC_1768
  unsigned int tout;

  LPC_EMAC->MADR=DP83848C_DEF_ADR | PhyReg;
  LPC_EMAC->MCMD=MCMD_READ;

  /* Wait until operation completed */
  tout=0;
  for(tout=0; tout < MII_RD_TOUT; tout++)
  {
    if((LPC_EMAC->MIND & MIND_BUSY)==0) break;
  }
  LPC_EMAC->MCMD=0;
  return(LPC_EMAC->MRDD);
#else
  // SOFTWARE MDIO CONTROL(SEE AN10859.pdf)
  return mdio_read(PhyReg);
#endif
}

// Keil: function added to initialize Rx Descriptors
void rx_descr_init(void)
{
  unsigned int i;

  for(i=0; i < NUM_RX_FRAG; i++)
  {
    RX_DESC_PACKET(i) =RX_BUF(i);
    RX_DESC_CTRL(i)   =RCTRL_INT |(ETH_FRAG_SIZE-1);
    RX_STAT_INFO(i)   =0;
    RX_STAT_HASHCRC(i)=0;
  }

  /* Set EMAC Receive Descriptor Registers. */
  LPC_EMAC->RxDescriptor   =RX_DESC_BASE;
  LPC_EMAC->RxStatus       =RX_STAT_BASE;
  LPC_EMAC->RxDescriptorNumber=NUM_RX_FRAG-1;

  /* Rx Descriptors Point to 0 */
  LPC_EMAC->RxConsumeIndex =0;
}

// Keil: function added to initialize Tx Descriptors
void tx_descr_init(void)
{
  unsigned int i;

  for(i=0; i < NUM_TX_FRAG; i++)
  {
    TX_DESC_PACKET(i)=TX_BUF(i);
    TX_DESC_CTRL(i)  =0;
    TX_STAT_INFO(i)  =0;
  }

  /* Set EMAC Transmit Descriptor Registers. */
  LPC_EMAC->TxDescriptor   =TX_DESC_BASE;
  LPC_EMAC->TxStatus       =TX_STAT_BASE;
  LPC_EMAC->TxDescriptorNumber=NUM_TX_FRAG-1;

  /* Tx Descriptors Point to 0 */
  LPC_EMAC->TxProduceIndex =0;
}

bool EthAutoNegotiation(void)
{
  unsigned int regv,tout,id1,id2;

  // Check if this is a DP83848C PHY
  id1=read_PHY(PHY_REG_IDR1);
  id2=read_PHY(PHY_REG_IDR2);
  if(((id1 << 16) | (id2 & 0xFFF0))!=DP83848C_ID) return(true);
  // Configure the PHY device
  write_PHY(PHY_REG_BMCR, PHY_AUTO_NEG); // Use autonegotiation about the link speed
  for(tout=1000;tout>0;tout--)
  { // Wait to complete Auto_Negotiation
    regv=read_PHY(PHY_REG_BMSR);
    if(regv & 0x0020)
    { // Autonegotiation Complete
      return(true);
    }
  }
  return(false);
}

void EthSpeedAndDuplexMode(unsigned int regv)
{
  // Configure Full/Half Duplex mode
  if(regv & 0x0004)
  { // Full duplex is enabled
    LPC_EMAC->MAC2    |= MAC2_FULL_DUP;
    LPC_EMAC->Command |= CR_FULL_DUP;
    LPC_EMAC->IPGT    =IPGT_FULL_DUP;
  }
  else
  { // Half duplex mode
    LPC_EMAC->IPGT=IPGT_HALF_DUP;
  }

  // Configure 100MBit/10MBit mode
  if(regv & 0x0002)
  { // 10MBit mode
    LPC_EMAC->SUPP=0;
  }
  else
  { // 100MBit mode
    LPC_EMAC->SUPP=SUPP_SPEED;
  }
}

void EthEnableRxTx(void)
{
  LPC_EMAC->Command  |=(CR_RX_EN | CR_TX_EN);
  LPC_EMAC->MAC1     |= MAC1_REC_EN;
  s_bLinkOn=true;
}

void EthDisableRxTx(void)
{
  LPC_EMAC->MAC1     &= ~MAC1_REC_EN;
  LPC_EMAC->Command  &=~(CR_RX_EN | CR_TX_EN);
  s_bLinkOn=false;
}

#define d_HalfDuplex    0x01
#define d_FullDuplex    0x02
#define d_Speed10       0x40
#define d_Speed100      0x80

bool EthCheckLinkStatus(void)
{
  unsigned int regv;

  if(!s_bInit_EMAC) return(false);
  regv=read_PHY(PHY_REG_STS);
  if(regv & 0x0001)
  { // Link is on
    WORD wSpeedDuplex;
    static WORD s_wSpeedDuplex=0;

    if(regv & 0x0004) wSpeedDuplex|=d_FullDuplex;
    else wSpeedDuplex|=d_HalfDuplex;
    if(regv & 0x0002) wSpeedDuplex|=d_Speed10;
    else wSpeedDuplex|=d_Speed100;
    if(!s_bLinkOn || (wSpeedDuplex!=s_wSpeedDuplex))
    {
      EthSpeedAndDuplexMode(regv);
      EthEnableRxTx();
      s_wSpeedDuplex=wSpeedDuplex;
    }
    return(true);
  }
  if(s_bLinkOn) EthDisableRxTx();
  return(false);
}

// configure port-pins for use with LAN-controller,
// reset it and send the configuration-sequence
bool Init_EMAC(unsigned char *pMAC)
{
// Keil: function modified to access the EMAC
// Initializes the EMAC ethernet controller
  unsigned int regv,tout;

  /* Power Up the EMAC controller. */
  LPC_SC->PCONP |= 0x40000000;
  /* Enable P1 Ethernet Pins. */
//  if(MAC_MODULEID==OLD_EMAC_MODULE_ID) { 
    /* For the first silicon rev.'-' ID P1.6 should be set. */
//   LPC_PINCON->PINSEL2=0x50151105;
 // }
 // else {
    /* on rev. 'A' and later, P1.6 should NOT be set. */
    LPC_PINCON->PINSEL2=0x50150105;
 // }
  LPC_PINCON->PINSEL3=(LPC_PINCON->PINSEL3 & ~0x0000000F) | 0x00000005;

  EthDisableRxTx();

  // Disable EMAC interrupts
  LPC_EMAC->IntEnable=0;
  // Reset all interrupts
  LPC_EMAC->IntClear =0xFFFF;

  // Reset all EMAC internal modules
  LPC_EMAC->MAC1=MAC1_RES_TX | MAC1_RES_MCS_TX | MAC1_RES_RX | MAC1_RES_MCS_RX | MAC1_SIM_RES | MAC1_SOFT_RES;
  // MAC_COMMAND=CR_REG_RES | CR_TX_RES | CR_RX_RES;
  LPC_EMAC->Command=CR_REG_RES | CR_TX_RES | CR_RX_RES | CR_PASS_RUNT_FRM;
  
  // A short delay after reset
  for(tout=10000; tout; tout--);

  /* Initialize MAC control registers. */
  LPC_EMAC->MAC1=MAC1_PASS_ALL;
  LPC_EMAC->MAC2=MAC2_CRC_EN | MAC2_PAD_EN; // | MAC2_VLAN_PAD_EN;

  LPC_EMAC->MAXF=ETH_MAX_FLEN;
  LPC_EMAC->CLRT=CLRT_DEF;
  LPC_EMAC->IPGR=IPGR_DEF;

  /* Enable Reduced MII interface. */
  LPC_EMAC->Command=CR_RMII | CR_PASS_RUNT_FRM;

  /* Put the DP83848C in reset mode */
  write_PHY(PHY_REG_BMCR, 0x8000);

  for(tout=100000;tout>0;tout--)
  { // Wait for hardware reset to end
    regv=read_PHY(PHY_REG_BMCR);
    if(!(regv & 0x8000))
    { // Reset complete
      break;
    }
  }
  if(!tout) return(false);

  EthAutoNegotiation();

  // Set the Ethernet MAC Address registers
  LPC_EMAC->SA0=(pMAC[5] << 8) | pMAC[4];
  LPC_EMAC->SA1=(pMAC[3] << 8) | pMAC[2];
  LPC_EMAC->SA2=(pMAC[1] << 8) | pMAC[0];

  // Initialize Tx and Rx DMA Descriptors
  rx_descr_init();
  tx_descr_init();

  // Receive Broadcast and Perfect Match Packets
  LPC_EMAC->RxFilterCtrl=RFC_BCAST_EN | RFC_PERFECT_EN;

  // Enable EMAC interrupts
  LPC_EMAC->IntEnable=INT_RX_DONE | INT_TX_DONE;
  // Reset all interrupts
  LPC_EMAC->IntClear =0xFFFF;

  s_bInit_EMAC=true;
  return(true);
}

// copies bytes from frame port to MCU-memory
// NOTES: * an odd number of byte may only be transfered
//          if the frame is read to the end!
//        * MCU-memory MUST start at word-boundary
void CopyFromFrame_EMAC(void *Dest, unsigned short Size)
{
  unsigned short *piDest;

  piDest=Dest;
  while(Size>1)
  {
    *piDest++=*rptr++;
    Size-=2;
  }
  if(Size)
  { // check for leftover byte...
    *((unsigned char *)piDest)=(unsigned char)(*rptr);
  }
}

// Reads the length of the received ethernet frame
unsigned short StartReadFrame(void)
{
  unsigned int idx=LPC_EMAC->RxConsumeIndex;

  rptr=(unsigned short *)RX_DESC_PACKET(idx);
  return((RX_STAT_INFO(idx) & RINFO_SIZE)-3);
}

void EndReadFrame(void)
{ // DMA free packet
  unsigned int idx=LPC_EMAC->RxConsumeIndex;

  if(++idx==NUM_RX_FRAG) idx=0;
  LPC_EMAC->RxConsumeIndex=idx;
}

unsigned int CheckFrameReceived(void)
{
  if(LPC_EMAC->RxProduceIndex!=LPC_EMAC->RxConsumeIndex) return(1);
  else return(0);
}

// requests space in EMAC memory for storing an outgoing frame
void RequestSend(unsigned short FrameSize)
{
  unsigned int idx;

  idx =LPC_EMAC->TxProduceIndex;
  tptr=(unsigned short *)TX_DESC_PACKET(idx);
  TX_DESC_CTRL(idx)=(FrameSize-1) | TCTRL_LAST | TCTRL_CRC;
}

// check if ethernet controller is ready to accept the
// frame we want to send
unsigned int Rdy4Tx(void)
{ // the ethernet controller transmits much faster than the CPU can load its buffers
  return(1);
}

// copies bytes from MCU-memory to frame port
// NOTES: * an odd number of byte may only be transfered
//          if the frame is written to the end!
//        * MCU-memory MUST start at word-boundary
void CopyToFrame_EMAC(void *Source, unsigned int Size)
{
  unsigned short *piSource;
  unsigned int idx;

  piSource=Source;
  Size=(Size+1) & 0xFFFE;    // round Size up to next even number
  while(Size>0)
  {
    *tptr++=*piSource++;
    Size-=2;
  }
  idx=LPC_EMAC->TxProduceIndex+1;
  if(idx==NUM_TX_FRAG) idx=0;
  LPC_EMAC->TxProduceIndex=idx;
}
