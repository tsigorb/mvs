/******************************************************************
 *****                                                        *****
 *****  Name: easyweb.h                                       *****
 *****  Ver.: 1.0                                             *****
 *****  Date: 07/05/2001                                      *****
 *****  Auth: Andreas Dannenberg                              *****
 *****        HTWK Leipzig                                    *****
 *****        university of applied sciences                  *****
 *****        Germany                                         *****
 *****  Func: header-file for easyweb.c                       *****
 *****                                                        *****
 ******************************************************************/

#ifndef __EASYWEB_H
#define __EASYWEB_H

#include "./uip/uip.h"
#include "./uip/uip_arp.h"

//*****************************************************************************

// A set of flags.  The flag bits are defined as follows:
#define FLAG_SYSTICK            1       // 1 -> An indicator that a SysTick interrupt has occurred.
#define FLAG_RXPKT              2       // 2 -> An RX Packet has been received.
//#define FLAG_TXPKT              4       // 4 -> A TX packet DMA transfer is pending.
//#define FLAG_RXPKTPEND          8       // 8 -> A RX packet DMA transfer is pending.
//#define FLAG_APPTICK            16      // 16 -> �������� ������ 1 ��
/*#define FLAG_WRDACFIFO          
#define FLAG_RDADCFIFO          */

//////////////////////////////////////////////////////////////////////////
// UIP Timers (in MS)
#define UIP_PERIODIC_TIMER_MS   500
#define UIP_ARP_TIMER_MS        10000

//////////////////////////////////////////////////////////////////////////
#pragma push
#pragma pack(1)

typedef struct
{
  BYTE btMAC_Addr[6];
  DWORD dwIp,dwMask;
  WORD wPort;
} InitEthUdpTp;

#pragma pop

//////////////////////////////////////////////////////////////////////////
#define CMD_SUCCESS         0   // Command is executed successfully. Sent by ISP handler only when
                                // command given by the host has been completely and successfully executed.
#define INVALID_COMMAND     1   // Invalid command.
#define SRC_ADDR_ERROR      2   // Source address is not on word boundary.
#define DST_ADDR_ERROR      3   // Destination address is not on a correct boundary.
#define SRC_ADDR_NOT_MAPPED 4   // Source address is not mapped in the memory map.
                                // Count value is taken into consideration where applicable.
#define DST_ADDR_NOT_MAPPED 5   // Destination address is not mapped in the memory map.
                                // Count value is taken into consideration where applicable.
#define COUNT_ERROR         6   // Byte count is not multiple of 4 or is not a permitted value.
#define INVALID_SECTOR      7   // Sector number is invalid or end sector number is greater than start sector number.
#define SECTOR_NOT_BLANK    8   // Sector is not blank.
#define SECTOR_NOT_PREPARED_FOR_WRITE_OPERATION 9 //Command to prepare sector for write operation was not executed.
#define COMPARE_ERROR       10  // Source and destination data not equal.
#define BUSY                11  // Flash programming hardware interface is busy.
#define PARAM_ERROR         12  // Insufficient number of parameters or invalid parameter.
#define ADDR_ERROR          13  // Address is not on word boundary.
#define ADDR_NOT_MAPPED     14  // Address is not mapped in the memory map.
                                // Count value is taken in to consideration where applicable.
#define CMD_LOCKED          15  // Command is locked.
#define INVALID_CODE        16  // Unlock code is invalid.
#define INVALID_BAUD_RATE   17  // Invalid baud rate setting.
#define INVALID_STOP_BIT    18  // Invalid stop bit setting.
#define CODE_READ_PROTECTION_ENABLED 19 // Code read protection enabled.

#define d_MVSSET_ERRFMT     -2
#define d_MVSSET_ERRWR      -1
#define d_MVSSET_EQ         0
#define d_MVSSET_OK         1
#define d_MVSSET_NOCMD      2

#define dRinging_SirenaOn           0
#define dTalk_SirenaOff             1
#define dBye_SirenaOff              2
#define dPassive_SirenaOff          3
#define dPassive_SirenaOffByTOut    4

#define dSirenaOnTOut               20000
#define dSirenaOffTOut              7000
#define dMaxLedTOut                 500
#define dMaxLedEthTOut              5000
#define d_MaxEthXchgTOut            30000

//#define d_EthInitTOut               10000

#define d_MaxIpTel                  3

#define d_LED_GREEN         0x00000080 // P0.7
#define d_TX_RX_SW          0x00000001 // P2.0
#define d_SIRENA_PIN        0x00000002 // P2.1
#define d_IP_DEF            0x00000100 // P2.8

//////////////////////////////////////////////////////////////////////////
extern uint32_t SystemFrequency;

extern volatile unsigned long g_ulFlags;

// A system tick counter, incremented every SYSTICKMS.
extern volatile DWORD g_dwTickCounter;

// Macro for accessing the Ethernet header information in the buffer.
extern u8_t *g_btUIPBufPtr;             // ����������� �� ������� ����� ����� � g_btUIPBuffer (������������ ��� DMA)

extern const BYTE g_btMAC_Default[6];

extern char g_IpTelNumbs[d_MaxIpTel][20];

extern u8_t g_btSrsIP[4],g_btSrsMAC[6];

extern int g_iCurState;

//////////////////////////////////////////////////////////////////////////
/*void InitOsc(void);
void InitPorts(void);*/

void *memfind(void *pData,int iDataLen,void *pExample,int iExampleLen);

void InitCancelSIP(void);
void LedGreenToogle(void);
void IpCallSirena(int iNewState);
bool IsCallIpTlfn(void *pvNetBuf,WORD wNetSz);
bool IsAnswerIpTlfn(PBYTE pbtNetBuf,WORD wNetSz);
bool IsOtboiIpTlfn(void *pvNetBuf,WORD wNetSz);
void IpSirenaTimerWork(void);

void SetUIP_IpMaskMAC(InitEthUdpTp *psInitData);
int StoreSettings2Flash(InitEthUdpTp *psInitData);
int ReadSettingsFromFlash(InitEthUdpTp *psInitData);
int MVSSettingsPacket(InitEthUdpTp *psInitData,PCHAR pchIpData,WORD wSize0);

#endif
