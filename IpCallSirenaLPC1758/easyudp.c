#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <LPC17xx.h>      // Keil: Register definition file for LPC17xx
#include "easyudp.h"
#include "EMAC.h"         // Keil: *.c -> *.h    // ethernet packet driver
#include "clock-arch.h"

///////////////////////////////////////////////////////////////////////////////
extern void *g_pUipAppData; // The g_pUipAppData pointer points to application data.
extern u16_t g_wUipLen;

///////////////////////////////////////////////////////////////////////////////
volatile int s_iLedTOut=dMaxLedTOut,
             s_iMaxLedTOut=dMaxLedTOut,
             s_iEthXchgTOut=d_MaxEthXchgTOut;

volatile DWORD g_dwTickCounter=0,g_ulFlags=0;
volatile long g_lARPTimer=0;

const BYTE g_btMAC_Default[6]={0x00,0x15,0x65,0x5b,0x99,0x52};

InitEthUdpTp g_sInitData=
{
{0,0,0,0,0,0},                    // btMAC_Addr[6]
0xc0a801ca,                       // dwIp
0xffffff00,                       // dwMask
6002                              // wPort
};

u8_t g_btSrsIP[4],g_btSrsMAC[6];

u8_t g_btUIPBuffer[UIP_BUFSIZE+3];
u8_t *g_btUIPBufPtr;                  // ����������� �� ������� ����� ����� � g_btUIPBuffer
#define d_UipEthHdr                   ((struct uip_eth_hdr *)g_btUIPBufPtr)

bool g_bInitEth=false;

int g_iCurState=dPassive_SirenaOff;

///////////////////////////////////////////////////////////////////////////////
void SysTick_Handler(void)
{ // SysTick interrupt happens every 1 ms
  g_dwTickCounter++;
  g_ulFlags|=FLAG_SYSTICK;
}

///////////////////////////////////////////////////////////////////////////////
u32_t clock_time(void)
{ return(g_dwTickCounter); }

void uip_send(PCHAR pchBuf,WORD wDataSz)
{
  DWORD DstIp;
  PBYTE pbtUDPDATA=&g_btUIPBufPtr[UIP_LLH_LEN+UIP_IPUDPH_LEN];

  memcpy(pbtUDPDATA,pchBuf,wDataSz);  // COPY DATA TO UDP PACKET
//pbtUDPDATA[16]=0xb9;
  DstIp=*((DWORD *)g_btSrsIP);
  PrepareSendUdpWithMAC(DstIp,g_sInitData.wPort,g_btSrsMAC,wDataSz);
}

void udp_data_rcv(void)
{
  int iRes;

  iRes=MVSSettingsPacket(&g_sInitData,(PCHAR)g_pUipAppData,g_wUipLen);
  if(iRes!=d_MVSSET_NOCMD)
  {
    PCHAR strBadSet="MVS settings error!",
          strOkSet="MVS settings ok!";
//strOkSet="UDP test packet N1";

      if(iRes==d_MVSSET_ERRWR) uip_send(strBadSet,strlen(strBadSet));
      if((iRes==d_MVSSET_OK) || (iRes==d_MVSSET_EQ)) uip_send(strOkSet,strlen(strOkSet));
      if(iRes!=d_MVSSET_EQ)
      {
        g_bInitEth=true;
      }
      return;
    }
    switch(g_iCurState)
    {
      case dPassive_SirenaOff:
        if(IsCallIpTlfn(g_pUipAppData,g_wUipLen)) IpCallSirena(dRinging_SirenaOn);
        else
        {
          if(IsAnswerIpTlfn(g_pUipAppData,g_wUipLen))
          {
            IpCallSirena(dTalk_SirenaOff);
          }
        }
        break;
      case dPassive_SirenaOffByTOut:
      case dRinging_SirenaOn:
        if(IsAnswerIpTlfn(g_pUipAppData,g_wUipLen)) IpCallSirena(dTalk_SirenaOff);
        else
        {
          if(IsOtboiIpTlfn(g_pUipAppData,g_wUipLen)) IpCallSirena(dPassive_SirenaOff);
        }
        break;
      case dTalk_SirenaOff:
        if(!IsAnswerIpTlfn(g_pUipAppData,g_wUipLen)) IpCallSirena(dPassive_SirenaOff);
        break;
    }
}

void sleep(DWORD dw_msec)
{
  dw_msec+=g_dwTickCounter;
  while(g_dwTickCounter!=dw_msec) ;
}

int main(void)
{
  SystemInit();                                      // setup core clocks
  SysTick_Config(SystemFrequency/1000);              // Generate interrupt every 1 ms

  // Adjust the pointer to be aligned on an odd half word address so that DMA can be used.
  g_btUIPBufPtr=(u8_t *)(((unsigned long)g_btUIPBuffer+3) & 0xfffffffe);

  LPC_GPIO2->FIODIR |= d_TX_RX_SW | d_SIRENA_PIN;
  LPC_GPIO2->FIOCLR=d_TX_RX_SW | d_SIRENA_PIN;

  memcpy(g_sInitData.btMAC_Addr,g_btMAC_Default,6);
  LPC_GPIO2->FIODIR &= ~d_IP_DEF;
  if(LPC_GPIO2->FIOPIN & d_IP_DEF)
  {
    if(ReadSettingsFromFlash(&g_sInitData)<0)
    { StoreSettings2Flash(&g_sInitData); }
  }
  g_bInitEth=true;
  s_iEthXchgTOut=d_MaxEthXchgTOut;
  while(1)
  {
    if(g_bInitEth)
    {
      g_bInitEth=false;
      InitCancelSIP();
      if(Init_EMAC(g_sInitData.btMAC_Addr)) SetUIP_IpMaskMAC(&g_sInitData);
      else s_iMaxLedTOut=dMaxLedEthTOut;
    }
    if(g_ulFlags & FLAG_SYSTICK)
    {
      if(!EthCheckLinkStatus()) s_iMaxLedTOut=dMaxLedEthTOut;
      else s_iMaxLedTOut=dMaxLedTOut;
      IpSirenaTimerWork();
      // LED TOut
      if(s_iLedTOut) s_iLedTOut--;
      else
      {
        LedGreenToogle();
        s_iLedTOut=s_iMaxLedTOut;
        if(s_iMaxLedTOut==dMaxLedEthTOut) g_bInitEth=true;
      }

      if(s_iEthXchgTOut) s_iEthXchgTOut--;
      else
      {
        LPC_GPIO2->FIOPIN^=d_TX_RX_SW;
        s_iEthXchgTOut=d_MaxEthXchgTOut;
        g_bInitEth=true;
      }
      g_ulFlags&=~FLAG_SYSTICK; // Clear the SysTick interrupt flag
    }
    g_wUipLen=0;
    if(CheckFrameReceived())
    {
      g_wUipLen=StartReadFrame();
      if(g_wUipLen &&(g_wUipLen<=UIP_BUFSIZE))
      {
        s_iEthXchgTOut=d_MaxEthXchgTOut;
        LedGreenToogle();
        CopyFromFrame_EMAC(g_btUIPBufPtr,g_wUipLen);
        EndReadFrame(); // release buffer in ethernet controller
        if(g_wUipLen)
        {
          if(d_UipEthHdr->type==HTONS(UIP_ETHTYPE_IP)) uip_udp_rcv();
          else
          {
            if(d_UipEthHdr->type==HTONS(UIP_ETHTYPE_ARP)) uip_arp_arpin();
            else g_wUipLen=0;
          }
        }
      }
      else g_wUipLen=0;
    }
    if(g_wUipLen>0)
    {
      RequestSend(g_wUipLen);
      if(Rdy4Tx()) CopyToFrame_EMAC(g_btUIPBufPtr,g_wUipLen);
      if(g_bInitEth) sleep(100);
    }
  }
}
