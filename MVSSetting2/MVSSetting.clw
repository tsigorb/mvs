; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CMVSSettingDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "MVSSetting.h"

ClassCount=3
Class1=CMVSSettingApp
Class2=CMVSSettingDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_MVSSETTING_DIALOG

[CLS:CMVSSettingApp]
Type=0
HeaderFile=MVSSetting.h
ImplementationFile=MVSSetting.cpp
Filter=N

[CLS:CMVSSettingDlg]
Type=0
HeaderFile=MVSSettingDlg.h
ImplementationFile=MVSSettingDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=IDC_IPADDRESS

[CLS:CAboutDlg]
Type=0
HeaderFile=MVSSettingDlg.h
ImplementationFile=MVSSettingDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=?
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_MVSSETTING_DIALOG]
Type=1
Class=?
ControlCount=17
Control1=IDC_IPADDR_OLD,SysIPAddress32,1342242816
Control2=IDC_IPADDR_NEW,SysIPAddress32,1342242816
Control3=IDC_ED_MACNEW,edit,1350631432
Control4=IDC_ED_TEL1,edit,1350639616
Control5=IDC_ED_TEL2,edit,1350639616
Control6=IDC_ED_TEL3,edit,1350639616
Control7=IDOK,button,1342242817
Control8=IDC_BTN_CLR,button,1342251008
Control9=IDCANCEL,button,1342242816
Control10=IDC_STATIC,static,1342308352
Control11=IDC_STATIC,static,1342308864
Control12=IDC_STATIC,static,1342308864
Control13=IDC_STATIC,static,1342308864
Control14=IDC_STATIC,static,1342308352
Control15=IDC_STATIC,static,1342308352
Control16=IDC_ED_MACSRC,edit,1350631432
Control17=IDC_STATIC,static,1342308352

