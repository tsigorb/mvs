// MVSSetting.h : main header file for the MVSSETTING application
//

#if !defined(AFX_MVSSETTING_H__E024AB66_41FE_4324_A401_515772696CEA__INCLUDED_)
#define AFX_MVSSETTING_H__E024AB66_41FE_4324_A401_515772696CEA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CMVSSettingApp:
// See MVSSetting.cpp for the implementation of this class
//

class CMVSSettingApp : public CWinApp
{
public:
	CMVSSettingApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMVSSettingApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CMVSSettingApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MVSSETTING_H__E024AB66_41FE_4324_A401_515772696CEA__INCLUDED_)
