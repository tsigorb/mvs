// MVSSettingDlg.cpp : implementation file
#include "stdafx.h"
#include "MVSSetting.h"
#include "MVSSettingDlg.h"

#include <sys/stat.h>
#include <io.h>
#include <fcntl.h>
#include <string.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
PCHAR strWARN="��������",strERROR="������";

char g_strMAC_PC[13];
DWORD g_dwIP_DEF;

volatile BOOL g_bExit=FALSE;
volatile int g_iMVSAnswer=0;
SOCKET g_wsaSInet=INVALID_SOCKET;

/////////////////////////////////////////////////////////////////////////////
/*#include<nb30.h>

typedef struct
{
  ADAPTER_STATUS adapt;
  NAME_BUFFER NameBuff[30];
} ASTAT, * PASTAT;

CString GetMacAddress(CString sNetBiosName)
{
    ASTAT Adapter;
	
    NCB ncb;
    UCHAR uRetCode;
	
    memset(&ncb, 0, sizeof(ncb));
    ncb.ncb_command = NCBRESET;
    ncb.ncb_lana_num = 0;
	
    uRetCode = Netbios(&ncb);
	
    memset(&ncb, 0, sizeof(ncb));
    ncb.ncb_command = NCBASTAT;
    ncb.ncb_lana_num = 0;
	
    sNetBiosName.MakeUpper();
	
    FillMemory(ncb.ncb_callname, NCBNAMSZ - 1, 0x20);
	
    strcpy((char *)ncb.ncb_callname, (LPCTSTR) sNetBiosName);
	
    ncb.ncb_callname[sNetBiosName.GetLength()] = 0x20;
    ncb.ncb_callname[NCBNAMSZ] = 0x0;
	
    ncb.ncb_buffer = (unsigned char *) &Adapter;
    ncb.ncb_length = sizeof(Adapter);
	
    uRetCode = Netbios(&ncb);
    
    CString sMacAddress;
	
    if (uRetCode == 0)
    {
    	sMacAddress.Format(_T("%02x%02x%02x%02x%02x%02x"),
    	    Adapter.adapt.adapter_address[0],
            Adapter.adapt.adapter_address[1],
            Adapter.adapt.adapter_address[2],
            Adapter.adapt.adapter_address[3],
            Adapter.adapt.adapter_address[4],
            Adapter.adapt.adapter_address[5]);
    }
    return sMacAddress;
}*/

BOOL IsCorrectMACAddr(PCHAR strMAC)
{
  int i,iLen=strlen(strMAC);
  PCHAR strHEX="0123456789ABCDEF";

  if((iLen<12) || (iLen>13)) return(FALSE);
  if(iLen==13)
  {
    strMAC[12]=0;
  }
  strupr(strMAC);
  for(i=0;i<12;i++)
  {
    if(!strchr(strHEX,strMAC[i])) return(FALSE);
  }
  return(true);
}

BOOL IsCorrectIPAddr(PCHAR strIP)
{
  int i,iLn;
  char chBuf[4];
  PCHAR pchS,pchE;

  pchS=strIP;
  for(i=0;i<4;i++)
  {
    memset(chBuf,0,4);
    if(i!=3) pchE=strchr(pchS,'.');
    else pchE=pchS+strlen(pchS);
    if(!pchE) return(FALSE);
    iLn=pchE-pchS;
    if(!iLn || (iLn>4)) return(FALSE);
    strncpy(chBuf,pchS,iLn);
    chBuf[3]=0;
    iLn=atoi(chBuf);
    if((iLn<=0) || (iLn>254)) return FALSE;
    pchS=pchE+1;
  }
  return(TRUE);
}

DWORD str2ip(PCHAR strIP)
{
  int i,iLn;
  DWORD dwIP;
  char chBuf[4];
  PCHAR pchS,pchE;

  dwIP=0;
  pchS=strIP;
  for(i=0;i<4;i++)
  {
    memset(chBuf,0,4);
    if(i!=3) pchE=strchr(pchS,'.');
    else pchE=pchS+strlen(pchS);
    iLn=pchE-pchS;
    strncpy(chBuf,pchS,iLn);
    chBuf[3]=0;
    iLn=atoi(chBuf);
    dwIP=(dwIP << 8) | iLn;
    pchS=pchE+1;
  }
  return(dwIP);
}

void ReadMVSCfg(void)
{
#define d_PARAM_CNT         2

  int i;
  char chBuf[MAX_PATH],
       *pch1,
       *strParName[d_PARAM_CNT]=
{
"MAC_PC=",
"IP_DEF="
};
  FILE *ff1;

  memset(g_strMAC_PC,0,13);
  g_dwIP_DEF=0xc0a801ca;

  ::GetModuleFileName(NULL,chBuf,_MAX_PATH-40);
  pch1=strrchr(chBuf,'\\'); *pch1=0;
  sprintf(chBuf,"%s\\%s",chBuf,"mvs.cfg");
  ff1=fopen(chBuf,"r");
  if(!ff1) return;
  while(!feof(ff1))
  {
    if(fgets(chBuf,80,ff1))
    {
      for(i=0;i<d_PARAM_CNT;i++)
      {
        pch1=strstr(chBuf,strParName[i]);
        if(pch1 && (pch1==chBuf))
        {
          pch1=chBuf+strlen(strParName[i]);
          switch(i)
          {
            case 0:
            { // MAC_PC
              if(IsCorrectMACAddr(pch1)) memcpy(g_strMAC_PC,pch1,12);
              break;
            }
            case 1:
            { // IP_DEF
              if(IsCorrectIPAddr(pch1)) g_dwIP_DEF=str2ip(pch1);
              break;
            }
          }
        }
      }
    }
  }
  fclose(ff1);
}

DWORD WINAPI SocketRcvThread(LPVOID pObj)
{
  int iRes;
  char chBuf[2048];
  PCHAR strBadSet="MVS settings error!",
        strOkSet= "MVS settings ok!";
  fd_set sRcvS;
  timeval sTmV={0,10000};

  FD_ZERO(&sRcvS);
  while(!g_bExit)
  {
    FD_SET(g_wsaSInet,&sRcvS);
    if(select(0,&sRcvS,NULL,NULL,&sTmV)!=SOCKET_ERROR)
    {
      if(FD_ISSET(g_wsaSInet,&sRcvS))
      {
        memset(chBuf,0,2048);
        iRes=recv(g_wsaSInet,chBuf,2048,0);
        if(iRes!=SOCKET_ERROR)
        {
          if(strstr(chBuf,strBadSet)) g_iMVSAnswer=-1;
          if(strstr(chBuf,strOkSet)) g_iMVSAnswer=1;
        }
      }
    }
    else Sleep(100);
  }
  return(0);
}

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About
class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMVSSettingDlg dialog
CMVSSettingDlg::CMVSSettingDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMVSSettingDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMVSSettingDlg)
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMVSSettingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMVSSettingDlg)
	DDX_Control(pDX, IDC_IPADDR_NEW, m_cIpAddrNew);
	DDX_Control(pDX, IDC_IPADDR_OLD, m_cIpAddrOld);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CMVSSettingDlg, CDialog)
	//{{AFX_MSG_MAP(CMVSSettingDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BTN_CLR, OnBtnClr)
	ON_WM_TIMER()
	ON_EN_CHANGE(IDC_ED_MACNEW, OnChangeEdMacNew)
	ON_EN_CHANGE(IDC_ED_MACSRC, OnChangeEdMacSrc)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
BOOL GetInfoElement(PCHAR pchBuf,LPCSTR strInfo,int iNumbEl)
{
  int iCurEl,iLn;
  PCHAR pchStart,pchFinish;

  pchStart=(PCHAR)strInfo;
  for(iCurEl=0;iCurEl<=iNumbEl;iCurEl++)
  {
    pchStart=strchr(pchStart,'=');
    if(!pchStart) return(FALSE);
    pchStart++;
  }
  pchFinish=strchr(pchStart,',');
  if(!pchFinish) pchFinish=strchr(pchStart,'.');
  if(!pchFinish) return(FALSE);
  if((pchStart+1)==pchFinish) return(FALSE);
  iLn=pchFinish-pchStart;
  memcpy(pchBuf,pchStart,iLn);
  pchBuf[iLn]=0;
  return(TRUE);
}

BYTE Simb2Code(char chSimb)
{
  if((chSimb>='0') && (chSimb<='9')) return (BYTE)(chSimb-'0');
  if((chSimb>='a') && (chSimb<='f')) return (BYTE)(chSimb-'a'+10);
  if((chSimb>='A') && (chSimb<='F')) return (BYTE)(chSimb-'A'+10);
  return(0xff);
}

DWORD ElementCalcCRC(PCHAR pchElement)
{
  int i,iLn=strlen(pchElement);
  DWORD dwCRC=0;

  for(i=0;i<iLn;i++) dwCRC+=~Simb2Code(pchElement[i]);
  return(dwCRC);
}

DWORD CalcCRC(LPCSTR strInfo)
{
  DWORD dwCRC=0;
  char chBuf[30];

  if(GetInfoElement(chBuf,strInfo,2)) dwCRC+=ElementCalcCRC(chBuf);
  dwCRC&=0xffff;
  if(GetInfoElement(chBuf,strInfo,3)) dwCRC+=ElementCalcCRC(chBuf);
  dwCRC&=0xffff;
  if(GetInfoElement(chBuf,strInfo,4)) dwCRC+=ElementCalcCRC(chBuf);
  dwCRC&=0xffff;
  if(GetInfoElement(chBuf,strInfo,0)) dwCRC|=ElementCalcCRC(chBuf) << 16;
  return(dwCRC);
}

void CMVSSettingDlg::SaveData2MVS(BOOL bReset)
{
  int iRes;
  DWORD dwIpAddr0,dwIpAddr,dwCRC;
  SOCKADDR_IN sInet;
  CString cStr,cStrSend,cStrMac,cStrSRCMAC,cStrTel1,cStrTel2,cStrTel3;

  m_cIpAddrOld.GetAddress(dwIpAddr0);
  if(!dwIpAddr0)
  {
    MessageBox("����������� IP-����� ���!",strERROR,MB_ICONERROR);
    return;
  }
  m_cIpAddrNew.GetAddress(dwIpAddr);
  GetDlgItemText(IDC_ED_MACNEW,cStrMac);
  GetDlgItemText(IDC_ED_MACSRC,cStrSRCMAC);
  GetDlgItemText(IDC_ED_TEL1,cStrTel1);
  GetDlgItemText(IDC_ED_TEL2,cStrTel2);
  GetDlgItemText(IDC_ED_TEL3,cStrTel3);
  if(!dwIpAddr || (cStrMac.GetLength()!=12) || (cStrSRCMAC.GetLength()!=12) ||
     (!bReset && !(cStrTel1.GetLength() || cStrTel2.GetLength() || cStrTel3.GetLength())))
  {
    MessageBox("������ ������� �� ��������� ��� �� ���������!",strERROR,MB_ICONERROR);
    return;
  }
  cStr.Format("MVS settings: IPADDR=0x%08x,MAC=%s,TEL1=%s,TEL2=%s,TEL3=%s,SRCMAC=%s,CRC",
              dwIpAddr,cStrMac,cStrTel1,cStrTel2,cStrTel3,cStrSRCMAC);
  dwCRC=CalcCRC(cStr);
  cStrSend.Format("%s=0x%08x.",cStr,dwCRC);
  g_iMVSAnswer=0;
  m_iAnswerWait=20;
  SetTimer(IDD,100,NULL);
  sInet.sin_family=AF_INET;
  sInet.sin_addr.S_un.S_addr=htonl(dwIpAddr0);
  sInet.sin_port=htons(6002);
  iRes=sendto(g_wsaSInet,cStrSend,cStrSend.GetLength(),0,(PSOCKADDR)&sInet,sizeof(SOCKADDR_IN));
  if(iRes==SOCKET_ERROR)
  {
    KillTimer(IDD);
    MessageBox("������ �������� ���������� ��� ���!",strERROR,MB_ICONERROR);
  }
}
// "MVS settings: IPADDR=0xc0a801ca,MAC=0015655B9952,TEL1=5506,TEL2=,TEL3=,SRCMAC=ACDE48000081,CRC=0xfec2ffec."

/////////////////////////////////////////////////////////////////////////////
// CMVSSettingDlg message handlers
void CMVSSettingDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
  if((nID & 0xFFF0)==IDM_ABOUTBOX)
  {
    CAboutDlg dlgAbout;
    dlgAbout.DoModal();
  }
  else CDialog::OnSysCommand(nID, lParam);
}

void CMVSSettingDlg::OnPaint() 
{
  if(IsIconic())
  {
    CPaintDC dc(this);

    SendMessage(WM_ICONERASEBKGND,(WPARAM)dc.GetSafeHdc(),0);
    int cxIcon = GetSystemMetrics(SM_CXICON);
    int cyIcon = GetSystemMetrics(SM_CYICON);
    CRect rect;
    GetClientRect(&rect);
    int x=(rect.Width() - cxIcon + 1) / 2;
    int y=(rect.Height() - cyIcon + 1) / 2;

    dc.DrawIcon(x,y,m_hIcon);
  }
  else CDialog::OnPaint();
}

HCURSOR CMVSSettingDlg::OnQueryDragIcon()
{ return (HCURSOR) m_hIcon; }

BOOL CMVSSettingDlg::OnInitDialog()
{
  DWORD dwAddr,dwId;
  WSADATA wsData;
  BOOL bSockInit=FALSE;

  ReadMVSCfg();
  CDialog::OnInitDialog();
  ASSERT((IDM_ABOUTBOX & 0xFFF0)==IDM_ABOUTBOX);
  ASSERT(IDM_ABOUTBOX < 0xF000);

  CMenu *pSysMenu=GetSystemMenu(FALSE);
  if(pSysMenu!=NULL)
  {
    CString strAboutMenu;
    strAboutMenu.LoadString(IDS_ABOUTBOX);
    if(!strAboutMenu.IsEmpty())
    {
	    pSysMenu->AppendMenu(MF_SEPARATOR);
	    pSysMenu->AppendMenu(MF_STRING,IDM_ABOUTBOX,strAboutMenu);
    }
  }
  SetIcon(m_hIcon,TRUE);
  SetIcon(m_hIcon,FALSE);
  ((CEdit *)(GetDlgItem(IDC_ED_MACSRC)))->SetLimitText(12);
  ((CEdit *)(GetDlgItem(IDC_ED_MACNEW)))->SetLimitText(12);
  ((CEdit *)GetDlgItem(IDC_ED_TEL1))->SetLimitText(19);
  ((CEdit *)GetDlgItem(IDC_ED_TEL2))->SetLimitText(19);
  ((CEdit *)GetDlgItem(IDC_ED_TEL3))->SetLimitText(19);
  MVS_Clear();
  m_cIpAddrNew.GetAddress(dwAddr);
  m_cIpAddrOld.SetAddress(dwAddr);
  g_wsaSInet=INVALID_SOCKET;
  m_iAnswerWait=0;
  if(!WSAStartup(MAKEWORD(2,0),&wsData))
  {
    if(HIBYTE(wsData.wHighVersion)>=2)
    { // ������ WinSock >= 2.0
      g_wsaSInet=socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP);
      if(g_wsaSInet!=INVALID_SOCKET)
      {
        SOCKADDR_IN sInet;

        dwId=1;
//        setsockopt(g_wsaSInet,SOL_SOCKET,SO_BROADCAST,(PCHAR)&dwId,4);
        setsockopt(g_wsaSInet,SOL_SOCKET,SO_REUSEADDR,(PCHAR)&dwId,4);
        if(!ioctlsocket(g_wsaSInet,FIONBIO,&dwId)) // ������ ����� �������������
        {
          memset(&sInet,0,sizeof(SOCKADDR_IN));
          sInet.sin_family=AF_INET;
          sInet.sin_addr.S_un.S_addr=htonl(INADDR_ANY);
          sInet.sin_port=htons(6002);
          if(!bind(g_wsaSInet,(PSOCKADDR)&sInet,sizeof(SOCKADDR_IN)))
          {
            bSockInit=TRUE;
            m_hThread=CreateThread(NULL,0,SocketRcvThread,NULL,0,&dwId);
          }
        }
      }
    }
  }
  if(!bSockInit)
  {
    dwId=WSAGetLastError();
    if(g_wsaSInet!=INVALID_SOCKET) closesocket(g_wsaSInet);
    g_wsaSInet=INVALID_SOCKET;
    GetDlgItem(IDC_IPADDR_OLD)->EnableWindow(FALSE);
    GetDlgItem(IDC_IPADDR_NEW)->EnableWindow(FALSE);
    GetDlgItem(IDC_ED_TEL1)->EnableWindow(FALSE);
    GetDlgItem(IDC_ED_TEL2)->EnableWindow(FALSE);
    GetDlgItem(IDC_ED_TEL3)->EnableWindow(FALSE);
    GetDlgItem(IDOK)->EnableWindow(FALSE);
    GetDlgItem(IDC_BTN_CLR)->EnableWindow(FALSE);
    MessageBox("������ �������� ��� ������������� ������!",strERROR,MB_ICONERROR);
  }
  return TRUE;
}

void CMVSSettingDlg::OnOK() 
{
  if(MessageBox("�������� ������ � ���?",strWARN,MB_YESNO|MB_ICONWARNING)==IDYES) SaveData2MVS();
}

void CMVSSettingDlg::OnCancel() 
{
  if(MessageBox("����� �� ���������?",strWARN,MB_YESNO|MB_ICONWARNING)==IDYES)
  {
    if(m_hThread)
    {
      DWORD dwRes;
      g_bExit=TRUE;
      do
      {
        GetExitCodeThread(m_hThread,&dwRes);
        if(dwRes==STILL_ACTIVE) Sleep(10);
      }
      while(dwRes==STILL_ACTIVE);
      closesocket(g_wsaSInet);
    }
    CDialog::OnCancel();
    WSACleanup();
  }
}

// hpigorb Realtek MAC: FC15B404801C
void CMVSSettingDlg::MVS_Clear(void)
{
  m_cIpAddrNew.SetAddress(g_dwIP_DEF);
  SetDlgItemText(IDC_ED_MACSRC,g_strMAC_PC);
  SetDlgItemText(IDC_ED_MACNEW,"0015655b9952");
  SetDlgItemText(IDC_ED_TEL1,"");
  SetDlgItemText(IDC_ED_TEL2,"");
  SetDlgItemText(IDC_ED_TEL3,"");
}

void CMVSSettingDlg::OnBtnClr() 
{
  if(MessageBox("���������� ��������� ��� � �������� ���������?",strWARN,MB_YESNO|MB_ICONWARNING)==IDYES)
  {
    DWORD dwAddr;

    MVS_Clear();
    SaveData2MVS(TRUE);
    m_cIpAddrNew.GetAddress(dwAddr);
    m_cIpAddrOld.SetAddress(dwAddr);
  }
}

void CMVSSettingDlg::OnTimer(UINT nIDEvent) 
{
  CDialog::OnTimer(nIDEvent);
  if(nIDEvent!=IDD) return;
  if(g_iMVSAnswer!=0)
  {
    if(g_iMVSAnswer<0)
    {
      KillTimer(nIDEvent);
      MessageBox("������ ������ �������� �� ���� ���!",strERROR,MB_ICONERROR);
      m_iAnswerWait=0;
    }
    if(g_iMVSAnswer>0)
    {
      KillTimer(nIDEvent);
      MessageBox("��������� ��� ���������!",strWARN,MB_OK);
      m_iAnswerWait=0;
    }
  }
  if(m_iAnswerWait)
  {
    if(m_iAnswerWait==1)
    {
      KillTimer(nIDEvent);
      MessageBox("��� ������ �� ���!",strERROR,MB_ICONERROR);
    }
    m_iAnswerWait--;
  }
}

BOOL IsHexDigitSimbol(char chSimb)
{
  int i;
  char *strHex="0123456789ABCDEF";

  for(i=0;i<16;i++)
  {
    if(chSimb==strHex[i]) return(TRUE);
  }
  return(FALSE);
}

void CMVSSettingDlg::CorrectInputHexStr(int iDlgItem)
{
  int iLen;
  CString cStr;

  GetDlgItemText(iDlgItem,cStr);
  iLen=cStr.GetLength();
  if(iLen)
  {
    iLen--;
    if(!IsHexDigitSimbol(cStr.GetAt(iLen)))
    {
      SetDlgItemText(iDlgItem,cStr.Left(iLen));
      ((CEdit *)(GetDlgItem(iDlgItem)))->SetSel(0,-1);
      ((CEdit *)(GetDlgItem(iDlgItem)))->SetSel(-1,-1);
    }
  }
}

void CMVSSettingDlg::OnChangeEdMacNew() 
{
  CorrectInputHexStr(IDC_ED_MACNEW);
}

void CMVSSettingDlg::OnChangeEdMacSrc()
{
  CorrectInputHexStr(IDC_ED_MACSRC);
}
