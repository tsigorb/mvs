// MVSSettingDlg.h : header file
#if !defined(AFX_MVSSETTINGDLG_H__D3F0F1BB_C6CB_4D98_9C31_8AA22D654D72__INCLUDED_)
#define AFX_MVSSETTINGDLG_H__D3F0F1BB_C6CB_4D98_9C31_8AA22D654D72__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <Winsock2.h>

/////////////////////////////////////////////////////////////////////////////
// CMVSSettingDlg dialog
class CMVSSettingDlg : public CDialog
{
// Construction
public:
	CMVSSettingDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CMVSSettingDlg)
	enum { IDD = IDD_MVSSETTING_DIALOG };
	CIPAddressCtrl	m_cIpAddrNew;
	CIPAddressCtrl	m_cIpAddrOld;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMVSSettingDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
  HICON m_hIcon;
  int m_iAnswerWait;
  HANDLE m_hThread;

// functions
protected:
    void CorrectInputHexStr(int iDlgItem);

protected:
    void MVS_Clear(void);
    void SaveData2MVS(BOOL bReset=FALSE);

	// Generated message map functions
	//{{AFX_MSG(CMVSSettingDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnBtnClr();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnChangeEdMacNew();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnChangeEdMacSrc();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.
#endif // !defined(AFX_MVSSETTINGDLG_H__D3F0F1BB_C6CB_4D98_9C31_8AA22D654D72__INCLUDED_)
