#ifndef __MKTU_ETHERNET_H__
#define __MKTU_ETHERNET_H__

clock_time_t clock_time(void);
long EthernetPacketGetDMA(unsigned long ulBase, unsigned char *pucBuf, long lBufLen);
long EthernetPacketPutDMA(unsigned long ulBase,unsigned char *pucBuf);

#endif
