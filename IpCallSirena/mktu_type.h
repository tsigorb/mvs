#ifndef __MKTU_TYPE_H__
#define __MKTU_TYPE_H__

#define d_MaxIpTel          3

#pragma push
#pragma pack(1)

typedef union
{
DWORD dwData;
WORD wData[2];
BYTE btData[4];
} U_DW;

typedef struct
{
  BYTE btMAC_Addr[6];
  DWORD dwIpMktu,dwMask;
  WORD wPortMktuSend,wPortMktuRcv;
} InitDataTp;

#pragma pop

#endif
