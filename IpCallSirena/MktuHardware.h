#ifndef __MKTU_HARDWARE_H__
#define __MKTU_HARDWARE_H__

#include "inc/lm3s9b92.h"
#include "inc/hw_ethernet.h"
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_udma.h"
#include "driverlib/ethernet.h"
#include "driverlib/flash.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/sysctl.h"
#include "driverlib/systick.h"
#include "driverlib/udma.h"
//#include "driverlib/adc.h"
//#include "driverlib/timer.h"
//#include "driverlib/i2s.h"
//#include "driverlib/i2c.h"
//#include "driverlib/rom.h"
//#include "driverlib/debug.h"

#include "./uip/uip.h"
#include "./uip/uip_arp.h"

//*****************************************************************************
// Defines for setting up the system clock.
#define SYSTICKHZ               CLOCK_SECOND
#define SYSTICKMS               (1000 / SYSTICKHZ)
#define SYSTICKUS               (1000000 / SYSTICKHZ)
#define SYSTICKNS               (1000000000 / SYSTICKHZ)

// A set of flags.  The flag bits are defined as follows:
#define FLAG_SYSTICK            1       // 1 -> An indicator that a SysTick interrupt has occurred.
#define FLAG_RXPKT              2       // 2 -> An RX Packet has been received.
//#define FLAG_TXPKT              4       // 4 -> A TX packet DMA transfer is pending.
//#define FLAG_RXPKTPEND          8       // 8 -> A RX packet DMA transfer is pending.
//#define FLAG_APPTICK            16      // 16 -> �������� ������ 1 ��
/*#define FLAG_WRDACFIFO          
#define FLAG_RDADCFIFO          */

//////////////////////////////////////////////////////////////////////////
// I2S Pin definitions
// Port A
#define RST_CODEC_PIN           GPIO_PIN_1
#define I2S0_SDARX_PIN          GPIO_PIN_2
#define I2S0_MCLKRX_PIN         GPIO_PIN_3
#define I2S0_SCLKTX_PIN         GPIO_PIN_4

// Port D
#define I2S0_SCLKRX_PIN         GPIO_PIN_0
#define I2S0_WSRX_PIN           GPIO_PIN_1

// Port E
#define I2S0_WSTX_PIN           GPIO_PIN_4
#define I2S0_SDATX_PIN          GPIO_PIN_5

//////////////////////////////////////////////////////////////////////////
#define d_UipEthHdr             ((struct uip_eth_hdr *)g_btUIPBufPtr)

// UIP Timers (in MS)
#define UIP_PERIODIC_TIMER_MS   500
#define UIP_ARP_TIMER_MS        10000

// I2C ����� TLV320AIC104
#define d_AIC104_Addr           0x18

//*****************************************************************************
extern WORD g_wPortNumber;

extern volatile unsigned long g_ulFlags;

extern volatile int g_iCodecCnt;

// A system tick counter, incremented every SYSTICKMS.
extern volatile DWORD g_dwTickCounter;

// The control table used by the uDMA controller.  This table must be aligned to a 1024 byte boundary.
// In this application uDMA is only used for Ethenet, so only the first 8 channels are needed.
extern tDMAControlTable g_sDMAControlTable[8];

// Macro for accessing the Ethernet header information in the buffer.
extern u8_t *g_btUIPBufPtr;             // ����������� �� ������� ����� ����� � g_btUIPBuffer (������������ ��� DMA)

//*****************************************************************************
void Ethernet_Init(BYTE *pbtMACAddr);
void InitHardware(void);
void SetAgcParam(int iChan,BYTE btAgcParam);
void MktuSwitchSet(int iChan,BYTE btSwitch);

#endif
