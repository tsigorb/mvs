#include "MktuHardware.h"
#include "MktuEthernet.h"

// When using the timer module in UIP, this function is required to return the number of ticks.
// Note that the file "clock-arch.h" must be provided by the application,
// and define CLOCK_CONF_SECONDS as the number of ticks per second,  and must also define the typedef "clock_time_t".
clock_time_t clock_time(void)
{ return((clock_time_t)g_dwTickCounter); }

/*
// Read a packet using DMA instead of directly reading the FIFO if the alignment will allow it.
long EthernetPacketGetDMA(unsigned long ulBase, unsigned char *pucBuf, long lBufLen)
{
  unsigned long ulTemp;
  unsigned char pucData[4];
  unsigned char *pucBuffer;
  long lTempLen, lFrameLen;
  long lRemainder;
  int iIdx;

  // If the buffer is not aligned on an odd half-word then it cannot use DMA.
  // This is because the two packet length bytes are written in front of the packet,
  // and the packet data must have two bytes that can be pulled off to become a word
  // and leave the remainder of the buffer word aligned.
  if(((unsigned long)pucBuf & 3)!=2)
  {
    // If there is not proper alignment the packet must be sent without using DMA.
    return(EthernetPacketGet(ulBase, pucBuf, lBufLen));
  }
  // Read WORD 0 from the FIFO, set the receive Frame Length and store the
  // first two bytes of the destination address in the receive buffer.
  ulTemp=HWREG(ulBase+MAC_O_DATA);
  lFrameLen=(long)(ulTemp & 0xffff);
  pucBuf[0]=(unsigned char)((ulTemp >> 16) & 0xff);
  pucBuf[1]=(unsigned char)((ulTemp >> 24) & 0xff);
  // The maximum DMA size is the frame size-the two bytes already read and truncated to the nearest word size.
  lTempLen=(lFrameLen-2) & 0xfffffffc;
  lRemainder=(lFrameLen-2) & 3;
  // Don't allow writing beyond the end of the buffer.
  if(lBufLen<lTempLen)
  {
    lRemainder=lTempLen-lBufLen;
    lTempLen=lBufLen;
  }
  else
  {
    if(lBufLen>=(lFrameLen-2+3))
    { // If there is room, just DMA the last word as well so that the special copy after DMA is not required.
      lRemainder=0;
      lTempLen=lFrameLen-2+3;
    }
  }
  // Set up the DMA to transfer the Ethernet header when a packet is received
  uDMAChannelTransferSet(UDMA_CHANNEL_ETH0RX, UDMA_MODE_AUTO,(void *)(ETH_BASE+MAC_O_DATA), &pucBuf[2], lTempLen>>2);
  uDMAChannelEnable(UDMA_CHANNEL_ETH0RX);
  // Issue a software request to start the channel running.
  uDMAChannelRequest(UDMA_CHANNEL_ETH0RX);
  // Wait for the previous transmission to be complete.
  while(uDMAChannelModeGet(UDMA_CHANNEL_ETH0RX)!=UDMA_MODE_STOP)
  { }
  // See if there are extra bytes to read into the buffer.
  if(lRemainder)
  {
    if(lRemainder>3)
    { // If the remainder is more than 3 bytes then the buffer was never big enough and data must be tossed.
      while(lRemainder>0)
      { // Read any remaining WORDS (that did not fit into the buffer).
        ulTemp=HWREG(ulBase+MAC_O_DATA);
        lRemainder-=4;
      }
    }
    // Read the last word from the FIFO.
    *((unsigned long *)&pucData[0])=HWREG(ulBase+MAC_O_DATA);
    // The current buffer position is lTempLen plus the two bytes read from the first word.
    pucBuffer=&pucBuf[lTempLen+2];
    // Read off each individual byte and save it.
    for(iIdx=0; iIdx<lRemainder; iIdx++)
    { pucBuffer[iIdx]=pucData[iIdx]; }
  }
  lFrameLen-=6;
  if(lFrameLen>lBufLen) return(-lFrameLen);  // If frame was larger than the buffer, return the "negative" frame length.
  // Return the Frame Length
  return(lFrameLen);
}

// Transmit a packet using DMA instead of directly writing the FIFO if the alignment will allow it.
long EthernetPacketPutDMA(unsigned long ulBase,unsigned char *pucBuf)
{
  unsigned long ulTemp;
  long lBufLen=g_wUipLen;

//static DWORD s_dwMaxTx=0;

  g_wUipLen=0;
  // If the buffer is not aligned on an odd half-word then it cannot use DMA.
  // This is because the two packet length bytes are written in front of the packet,
  // and the packet data must have two bytes that can be pulled off
  // to become a word and leave the remainder of the buffer word aligned.
  if(((unsigned long)pucBuf & 3)!=2)
  { // If there is not proper aligment the packet must be sent without using DMA.
    //HWREGBITW(&g_ulFlags, FLAG_TXPKT)=0;
    return(EthernetPacketPut(ulBase, pucBuf, lBufLen));
  }
  // Build and write WORD 0 (see format above) to the transmit FIFO.
  ulTemp=(unsigned long)(lBufLen-14);
  ulTemp|=(*pucBuf++) << 16;
  ulTemp|=(*pucBuf++) << 24;
  HWREG(ulBase+MAC_O_DATA)=ulTemp;
  // Force an extra word to be transferred if the end of the buffer is not aligned on a word boundary.
  // The math is actually lBufLen-2+3 to insure that the proper number of bytes are written.
  lBufLen+=1;
  // Configure the TX DMA channel to transfer the packet buffer.
  uDMAChannelTransferSet(UDMA_CHANNEL_ETH0TX,UDMA_MODE_AUTO,
                         pucBuf,(void *)(ETH_BASE+MAC_O_DATA),lBufLen>>2);
  // Enable the Ethernet Transmit DMA channel.
  uDMAChannelEnable(UDMA_CHANNEL_ETH0TX);
  // Issue a software request to start the channel running.
  uDMAChannelRequest(UDMA_CHANNEL_ETH0TX);
  // Indicate that a packet is being sent.
//  HWREGBITW(&g_ulFlags, FLAG_TXPKT)=1;
//ulTemp=g_ulTickCounter;
  // Wait for the transmission to be complete.
  while((uDMAChannelModeGet(UDMA_CHANNEL_ETH0TX)!=UDMA_MODE_STOP) || (EthernetSpaceAvail(ETH_BASE)==FALSE))
  {  }
  // Trigger the transmission of the data.
  HWREG(ETH_BASE+MAC_O_TR)=MAC_TR_NEWTX;
///if(g_ulTickCounter>=ulTemp) ulTemp=g_ulTickCounter-ulTemp;
//else ulTemp=1+g_ulTickCounter+((DWORD)0xffffffff-ulTemp);
//if(ulTemp>s_dwMaxTx) s_dwMaxTx=ulTemp;
  // Take back off the byte that we addeded above.
  return(lBufLen-1);
}
*/
