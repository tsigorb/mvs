#include <string.h>
#include <stdio.h>

#include "MktuHardware.h"
#include "MktuEthernet.h"
#include "mktu.h"

//*****************************************************************************
#define dRinging_SirenaOn           0
#define dTalk_SirenaOff             1
#define dBye_SirenaOff              2
#define dPassive_SirenaOff          3
#define dPassive_SirenaOffByTOut    4

#define dSirenaOnTOut               20000
#define dSirenaOffTOut              7000
#define dMaxLedTOut                 500

#define d_EthInitTOut               10000

typedef struct
{
  unsigned short wSeq;
  int iCnt;
} RTP_SEQ_TP;

//*****************************************************************************
volatile unsigned long g_ulFlags;

volatile int g_iEthInitTOut=d_EthInitTOut;

volatile int g_iSirenaTOut=0,g_iRTP_TOut=0;

volatile int g_iLedTOut=dMaxLedTOut;

static int s_iCurState=dPassive_SirenaOff;

static BOOL bLedGreenOn=false;

BOOL g_bInitEth=true;
u8_t g_btUIPBuffer[UIP_BUFSIZE+3];
u8_t *g_btUIPBufPtr;       // ����������� �� ������� ����� ����� � g_btUIPBuffer (������������ ��� DMA)

// Macro for accessing the Ethernet header information in the buffer.
#define d_UipEthHdr             ((struct uip_eth_hdr *)g_btUIPBufPtr)

volatile u8_t g_bReverceInput=0;
volatile DWORD g_dwTickCounter=0;  // A system tick counter, incremented every SYSTICKMS
volatile long g_lARPTimer=0;

//////////////////////////////////////////////////////////////////////////
InitDataTp g_sInitData=
{
{0x00,0x15,0x65,0x5a,0x00,0x00},  // MAC-address (2 last bytes sets to 2 low bytes IP-address!)
0xc0a801ed,         // dwIpMktu (Ip-address IpTel)
0xffffff00,         // dwMask
0,                  // wPortMktuSend
5060                // wPortMktuRcv
};

char g_IpTelNumbs[d_MaxIpTel][20]=
{
"!!!!",
"!!!!",
"!!!!"
};

RTP_SEQ_TP g_sRTPSeq={0,0};

char s_MasCancelSip[d_MaxIpTel][32];

//*****************************************************************************
// The error routine that is called if the driver library encounters an error.
#ifdef DEBUG
void __error__(char *pcFilename, unsigned long ulLine)
{ }
#endif

// The interrupt handler for the SysTick interrupt.
void __irq SysTickIntHandler(void)
{
  // Increment the system tick count.
  g_dwTickCounter++;
  // Indicate that a SysTick interrupt has occurred.
  g_ulFlags|=FLAG_SYSTICK;
}

// The interrupt handler for the Ethernet interrupt.
void __irq EthernetIntHandler(void)
{
  unsigned long ulTemp;

  // Read and Clear the interrupt.
  ulTemp=EthernetIntStatus(ETH_BASE, false);
  EthernetIntClear(ETH_BASE, ulTemp);
/*  // Check to see if an RX Interrupt has occurred.
  if(ulTemp & ETH_INT_RX)
  {
    // Indicate that a packet has been received.
    g_ulFlags|=FLAG_RXPKT;
    // Disable Ethernet Interrupt.
    IntDisable(INT_ETH);
  }*/
}

///////////////////////////////////////////////////////////////////////////////
void InitCancelSIP(void)
{
  int i;
  char *strSipCmdCancel[1]=
{
"CANCEL sip:"
};

  for(i=0;i<d_MaxIpTel;i++)
  {
    sprintf(s_MasCancelSip[i],"%s%s",strSipCmdCancel[0],g_IpTelNumbs[i]);
  }
  // !!! ������������� ��� ����-��...
}

void LedGreenToogle(void)
{
  if(bLedGreenOn)
  { GPIOPinWrite(GPIO_PORTE_BASE,GPIO_PIN_0,0xff); bLedGreenOn=false; }
  else
  { GPIOPinWrite(GPIO_PORTE_BASE,GPIO_PIN_0,0x00); bLedGreenOn=true; }
}

void IpCallSirena(int iNewState)
{
  if((s_iCurState==dPassive_SirenaOff) && (iNewState==dRinging_SirenaOn))
  { // �������� ������
    g_iSirenaTOut=dSirenaOnTOut;
    g_iRTP_TOut=0;
    s_iCurState=dRinging_SirenaOn;
  }
  else
  {
    if(iNewState!=dRinging_SirenaOn)
    { // ��������� ������
      if(iNewState==dPassive_SirenaOffByTOut) g_iSirenaTOut=dSirenaOffTOut;
      else g_iSirenaTOut=0;
      g_sRTPSeq.iCnt=0;
      s_iCurState=iNewState;
    }
  }
  if(s_iCurState==dRinging_SirenaOn) GPIOPinWrite(GPIO_PORTD_BASE,GPIO_PIN_0,0xff);
  else GPIOPinWrite(GPIO_PORTD_BASE,GPIO_PIN_0,0);
}

/*
   1. ����� �������� 6150 (��������� ������)
� �������� ������ ������ ���� ��������� ������������������:
1.INVITE sip:6150@
2."To: <sip:6150@"
��� ����� �������� �������� ��� ���������������� ����������.

   2. ����� �������� 6150
recieve RTP-packets

   3. ����� ������
� �������� ������ ������ ���� ��������� ������������������:
1."CANCEL sip:6150"
*/

BOOL IsCallIpTlfn(void *pvNetBuf,WORD wNetSz)
{
  int i;
  char chBuf[32];

  for(i=0;i<d_MaxIpTel;i++)
  {
    if(g_bReverceInput)
    { strcpy(chBuf,"INVITE"); }
    else
    { sprintf(chBuf,"INVITE sip:%s@",g_IpTelNumbs[i]); }
    if(memfind(pvNetBuf,wNetSz,chBuf,strlen(chBuf)))
    {
      sprintf(chBuf,"To: <sip:%s@",g_IpTelNumbs[i]);
      if(memfind(pvNetBuf,wNetSz,chBuf,strlen(chBuf)))
      { return(true); }
    }
  }
  return(false);
}

BOOL IsAnswerIpTlfn(PBYTE pbtNetBuf,WORD wNetSz)
{
  // !!!CHECK RTP-header format!!!
  if((pbtNetBuf[0] & 0xc0)==0x80)
  {
    WORD wSeq;

    wSeq=pbtNetBuf[2];
    wSeq=(wSeq << 8) | pbtNetBuf[3];
    if(g_sRTPSeq.iCnt)
    {
      g_sRTPSeq.wSeq++;
      if(g_sRTPSeq.wSeq==wSeq)
      {
        if(g_sRTPSeq.iCnt==3)
        { g_iRTP_TOut=5; return(true); }
        g_sRTPSeq.iCnt++;
      }
      else g_sRTPSeq.iCnt=0;
    }
    else
    {
      g_sRTPSeq.iCnt=1;
      g_sRTPSeq.wSeq=wSeq;
    }
  }
  return(false);
}

BOOL IsOtboiIpTlfn(void *pvNetBuf,WORD wNetSz)
{
  int i;

  if(g_bReverceInput)
  {
    int i;
    char chBuf[32],
         *strReqTerm="Request Terminated",
         *strBusyHere="Busy Here";

    if(memfind(pvNetBuf,wNetSz,strReqTerm,strlen(strReqTerm)) ||
       memfind(pvNetBuf,wNetSz,strBusyHere,strlen(strBusyHere)))
    {
      for(i=0;i<d_MaxIpTel;i++)
      {
        sprintf(chBuf,"To: <sip:%s@",g_IpTelNumbs[i]);
        if(memfind(pvNetBuf,wNetSz,chBuf,strlen(chBuf)))
        { return(true); }
      }
    }
  }
  else
  {
    for(i=0;i<d_MaxIpTel;i++)
    {
      if(memfind(pvNetBuf,wNetSz,s_MasCancelSip[i],strlen(s_MasCancelSip[i]))) return(true);
    }
  }
  return(false);
}

/* In uip-conf.h we have defined the UIP_APPCALL macro to mktu_appcall so that this funcion is uIP's application function.
   This function is called whenever an uIP event occurs (e.g. when a new connection is established, new data arrives,
   sent data is acknowledged, data needs to be retransmitted, etc.). */
void mktu_appcall(void)
{
  int iRes;

  /* The uip_conn structure has a field called "appstate" that holds
     the application state of the connection. We make a pointer to this to access it easier.*/
  if(uip_newdata())
  {
    g_iEthInitTOut=d_EthInitTOut;
    iRes=MVSSettingsPacket((PCHAR)g_pUipAppData,g_wUipLen);
    if(iRes!=d_MVSSET_NOCMD)
    {
      PCHAR strBadSet="MVS settings error!",
            strOkSet="MVS settings ok!";
      if(iRes==d_MVSSET_ERRWR) uip_send(strBadSet,strlen(strBadSet));
      if((iRes==d_MVSSET_OK) || (iRes==d_MVSSET_EQ)) uip_send(strOkSet,strlen(strOkSet));
      if(iRes!=d_MVSSET_EQ) g_bInitEth=true;
      return;
    }
    switch(s_iCurState)
    {
      case dPassive_SirenaOff:
        if(IsCallIpTlfn(g_pUipAppData,g_wUipLen)) IpCallSirena(dRinging_SirenaOn);
        else
        {
          if(IsAnswerIpTlfn(g_pUipAppData,g_wUipLen))
          {
            IpCallSirena(dTalk_SirenaOff);
            g_iEthInitTOut>>=3;
          }
        }
        break;
      case dPassive_SirenaOffByTOut:
      case dRinging_SirenaOn:
        if(IsAnswerIpTlfn(g_pUipAppData,g_wUipLen)) IpCallSirena(dTalk_SirenaOff);
        else
        {
          if(IsOtboiIpTlfn(g_pUipAppData,g_wUipLen)) IpCallSirena(dPassive_SirenaOff);
        }
        break;
      case dTalk_SirenaOff:
        g_iEthInitTOut>>=3;
        if(!IsAnswerIpTlfn(g_pUipAppData,g_wUipLen)) IpCallSirena(dPassive_SirenaOff);
        break;
    }
  }
}

//PCHAR strTEST="MVS settings: IPADDR=0xc0a801ca,TEL1=5002,TEL2=,TEL3=,CRC=0xfec2fff5.";

///////////////////////////////////////////////////////////////////////////////
int main(void)
{
  long lSz;

  // Adjust the pointer to be aligned on an odd half word address so that DMA can be used.
  g_btUIPBufPtr=(u8_t *)(((unsigned long)g_btUIPBuffer+3) & 0xfffffffe);

  InitHardware();
  if(uip_udp_new(g_sInitData.wPortMktuRcv,0,g_sInitData.wPortMktuSend)<0)
  {
    while(1);
  }
  MktuReset();

  if(GPIOPinRead(GPIO_PORTF_BASE,GPIO_PIN_0))
  {
    if(ReadSettingsFromFlash()<0) StoreSettings2Flash();
  }
  g_bInitEth=true;
  g_lARPTimer=0;
  g_ulFlags=0;
  while(TRUE)
  {
    if(g_ulFlags & FLAG_SYSTICK)
    {
      g_ulFlags&=~FLAG_SYSTICK; // Clear the SysTick interrupt flag
      g_lARPTimer++;

      // Sirena TOut
      if(g_iSirenaTOut)
      {
        g_iSirenaTOut--;
        if(!g_iSirenaTOut)
        {
          if(s_iCurState==dPassive_SirenaOffByTOut) IpCallSirena(dPassive_SirenaOff);
          else IpCallSirena(dPassive_SirenaOffByTOut);
        }
      }

      // RTP TOut
      if(g_iRTP_TOut)
      {
        g_iRTP_TOut--;
        if(!g_iRTP_TOut) IpCallSirena(dPassive_SirenaOff);
      }

      // LED TOut
      if(g_iLedTOut) g_iLedTOut--;
      else
      { LedGreenToogle(); g_iLedTOut=dMaxLedTOut; }

      // Ethernet Init TOut
      if(g_iEthInitTOut) g_iEthInitTOut--;
      else
      {
         g_iEthInitTOut=d_EthInitTOut;
         //Ethernet_Init(g_sInitData.btMAC_Addr);
         //if(s_iCurState!=dRinging_SirenaOn) IpCallSirena(dPassive_SirenaOff);
      }
    }
    if(g_bInitEth)
    {
      SysCtlDelay(500);
      InitCancelSIP();
      SetMktuIpMacAddrMask(&g_sInitData);
      Ethernet_Init(g_sInitData.btMAC_Addr);
      g_bInitEth=false;
    }
    // Check for an RX Packet and read it.
    if(EthernetPacketAvail(ETH_BASE))
    { // Clear the RX Packet event and re-enable RX Packet interrupts
      LedGreenToogle();
      lSz=EthernetPacketGet(ETH_BASE, g_btUIPBufPtr, UIP_BUFSIZE); // Get the packet and set g_wUipLen for uIP stack usage
      if(lSz<0) g_wUipLen=(unsigned short)(-lSz);
      else g_wUipLen=(unsigned short)lSz;
      if(g_wUipLen)
      {
        if(d_UipEthHdr->type==HTONS(UIP_ETHTYPE_IP))
        { // Process incoming IP packets here
          uip_arp_ipin();
          uip_process(UIP_DATA);
          if(g_wUipLen>0) uip_arp_out();
        }
        else
        {
          if(d_UipEthHdr->type==HTONS(UIP_ETHTYPE_ARP))
          { // Process incoming ARP packets here
            uip_arp_arpin();
          }
        }
        if(g_wUipLen>0)
        {
          EthernetPacketPut(ETH_BASE,g_btUIPBufPtr,g_wUipLen);
          g_wUipLen=0;
        }
      }
    }
    if(g_lARPTimer>=UIP_ARP_TIMER_MS)
    { // Process ARP Timer here
      uip_arp_timer();
      g_lARPTimer=0;
    }
  }
}
