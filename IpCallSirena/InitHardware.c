#include "MktuHardware.h"
#include <string.h>

//////////////////////////////////////////////////////////////////////////
void uDMA_Init(void)
{
/*
  // Enable the uDMA controller and set up the control table base.
  SysCtlPeripheralEnable(SYSCTL_PERIPH_UDMA);
  SysCtlDelay(10);
  uDMAEnable();
  uDMAControlBaseSet(g_sDMAControlTable);
  // Configure the DMA TX channel
  uDMAChannelAttributeDisable(UDMA_CHANNEL_ETH0TX,UDMA_ATTR_ALL);
  uDMAChannelControlSet(UDMA_CHANNEL_ETH0TX, UDMA_SIZE_32|UDMA_SRC_INC_32|UDMA_DST_INC_NONE|UDMA_ARB_8);
  // Configure the DMA channel for Ethernet receive.
  uDMAChannelAttributeDisable(UDMA_CHANNEL_ETH0RX, UDMA_ATTR_ALL);
  uDMAChannelControlSet(UDMA_CHANNEL_ETH0RX,
                        UDMA_SIZE_32 | UDMA_SRC_INC_NONE | UDMA_DST_INC_32 | UDMA_ARB_8);
*/
}

void Ethernet_Init(BYTE *pbtMACAddr)
{
  unsigned long ulTemp;

  IntDisable(INT_ETH);
  // Enable and Reset the Ethernet Controller.
  SysCtlPeripheralEnable(SYSCTL_PERIPH_ETH);
  SysCtlDelay(10);
  SysCtlPeripheralReset(SYSCTL_PERIPH_ETH);
  SysCtlDelay(100);
  // Enable Port F for Ethernet LEDs.
  GPIOPinConfigure(GPIO_PF2_LED1);
  GPIOPinConfigure(GPIO_PF3_LED0);
  GPIOPinTypeEthernetLED(GPIO_PORTF_BASE, GPIO_PIN_2 | GPIO_PIN_3);
  EthernetDisable(ETH_BASE);
  // Initialize the Ethernet Controller and disable all Ethernet Controller interrupt sources.
  ulTemp=SysCtlClockGet();
  EthernetInitExpClk(ETH_BASE,ulTemp);
  EthernetIntDisable(ETH_BASE,ETH_INT_PHY|ETH_INT_MDIO|ETH_INT_RXER|ETH_INT_RXOF|ETH_INT_TX|ETH_INT_TXER|ETH_INT_RX);
  ulTemp=EthernetIntStatus(ETH_BASE, false);
  EthernetIntClear(ETH_BASE, ulTemp);
  // Initialize the Ethernet Controller for operation.
  // Configure the Ethernet Controller for normal operation:
  //-Full Duplex
  //-TX CRC Auto Generation
  //-TX Padding Enabled
  //-Disable reception of packets with a bad CRC
//  EthernetConfigSet(ETH_BASE,ETH_CFG_RX_PRMSEN|ETH_CFG_RX_BADCRCDIS);
EthernetConfigSet(ETH_BASE,ETH_CFG_TX_DPLXEN|ETH_CFG_TX_CRCEN|ETH_CFG_TX_PADEN|ETH_CFG_RX_PRMSEN|ETH_CFG_RX_BADCRCDIS);
  // Program the hardware with it's MAC address (for filtering).
  EthernetMACAddrSet(ETH_BASE,pbtMACAddr);
  // Enable the Ethernet Controller.
  EthernetEnable(ETH_BASE);
// Wait for the link to become active.
//do
{ 
//ulTemp=EthernetPHYRead(ETH_BASE,PHY_MR0);
}
//while((ulTemp & 0x0004)==0);
  // Enable the Ethernet RX Packet interrupt source.
//  EthernetIntEnable(ETH_BASE, ETH_INT_RX);
  // Enable the Ethernet interrupt.
//  IntEnable(INT_ETH);
}

void GPIO_Enable(void)
{
//  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
//  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
//  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOG);
//  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOH);

  GPIOPinTypeGPIOOutput(GPIO_PORTD_BASE,GPIO_PIN_0);
  GPIOPinTypeGPIOOutput(GPIO_PORTE_BASE,GPIO_PIN_0);
  GPIOPinTypeGPIOInput(GPIO_PORTF_BASE,GPIO_PIN_0);
//  GPIOPinTypeGPIOOutput(GPIO_PORTB_BASE,GPIO_PIN_6);
//  GPIOPinTypeGPIOOutputOD(GPIO_PORTH_BASE,GPIO_PIN_0);
//  GPIOPinTypeGPIOOutput(GPIO_PORTH_BASE,GPIO_PIN_0);

  // !!!and more...

  GPIOPinWrite(GPIO_PORTD_BASE,GPIO_PIN_0,0);
  GPIOPinWrite(GPIO_PORTE_BASE,GPIO_PIN_0,0xff);
}

//////////////////////////////////////////////////////////////////////////
void InitHardware(void)
{
  // Set the clocking to run directly from the crystal.
//  SysCtlClockSet(SYSCTL_SYSDIV_10|SYSCTL_USE_PLL|SYSCTL_OSC_MAIN|SYSCTL_XTAL_16MHZ);
  SysCtlClockSet(SYSCTL_SYSDIV_4|SYSCTL_USE_PLL|SYSCTL_OSC_INT);
  FlashUsecSet(SysCtlClockGet()/SYSTICKUS);
  GPIO_Enable();
  SysTickPeriodSet(SysCtlClockGet()/SYSTICKMS); // Configure SysTick for a periodic interrupt
  SysTickEnable();
  SysTickIntEnable();
  uip_arp_init();
}
