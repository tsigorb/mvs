#include "MktuHardware.h"
#include "MktuEthernet.h"
#include "mktu.h"

//////////////////////////////////////////////////////////////////////////
extern InitDataTp g_sInitData;
/*{
{0x00,0x15,0x65,0x5a,0xad,0x37},  // MAC-address IpTel
0xc0a801ed,         // dwIpMktu (Ip-address IpTel)
0xffffff00,         // dwMask
0,                  // wPortMktuSend
5060                // wPortMktuRcv
};*/

extern char g_IpTelNumbs[d_MaxIpTel][20];

//////////////////////////////////////////////////////////////////////////
void MktuReset(void)
{
}

void SetMktuIpMacAddrMask(InitDataTp *psInitData)
{
  uip_ipaddr_t ipaddr;
  struct uip_eth_addr sMACAddr;

  memcpy(&sMACAddr.addr,psInitData->btMAC_Addr,6);
  uip_init();   // Initialize the uIP TCP/IP stack.
  uip_ipaddr(ipaddr,
             ((psInitData->dwIpMktu>>24) & 0xff),
             ((psInitData->dwIpMktu>>16) & 0xff),
             ((psInitData->dwIpMktu>>8) & 0xff),
             (psInitData->dwIpMktu & 0xff));
  uip_sethostaddr(ipaddr);
  uip_ipaddr(ipaddr,
             ((psInitData->dwMask>>24) & 0xff),
             ((psInitData->dwMask>>16) & 0xff),
             ((psInitData->dwMask>>8) & 0xff),
             (psInitData->dwMask & 0xff));
  uip_setnetmask(ipaddr);
  uip_setethaddr(sMACAddr);
}

//////////////////////////////////////////////////////////////////////////
#define d_SETTINGS2FLASH        0x20000
#define d_SETTINGSBUFSZ         (4+((sizeof(g_IpTelNumbs)>>2)<<2)+4+4)

void CreateMACFromIP(void)
{
  g_sInitData.btMAC_Addr[4]=(g_sInitData.dwIpMktu >> 8) & 0xff;
  g_sInitData.btMAC_Addr[5]=g_sInitData.dwIpMktu & 0xff;
}

DWORD CalcCRC4Flash(PBYTE pbtData,WORD wSz)
{
  DWORD dwCRC=0;

  while(wSz)
  {
    wSz--;
    dwCRC+=pbtData[wSz];
  }
  return(dwCRC);
}

int StoreSettings2Flash(void)
{
  WORD i;
  DWORD dwCRC;
  PBYTE pbtAddr=(PBYTE)d_SETTINGS2FLASH;
  static BYTE btSetBuf[d_SETTINGSBUFSZ];

  CreateMACFromIP();
  memcpy(btSetBuf,(void *)&g_sInitData.dwIpMktu,4);
  memcpy(btSetBuf+4,g_IpTelNumbs,sizeof(g_IpTelNumbs));
  dwCRC=CalcCRC4Flash(btSetBuf,d_SETTINGSBUFSZ-4);
  memcpy(btSetBuf+d_SETTINGSBUFSZ-4,&dwCRC,4);
  if(!memcmp(pbtAddr,btSetBuf,d_SETTINGSBUFSZ)) return(d_MVSSET_EQ);
  FlashErase((unsigned long)pbtAddr);
  for(i=0;i<1024;i++)
  {
    if(pbtAddr[i]!=0xff) return(d_MVSSET_ERRWR);
  }
  FlashProgram((unsigned long *)btSetBuf,(unsigned long)pbtAddr,d_SETTINGSBUFSZ);
  if(memcmp(pbtAddr,btSetBuf,d_SETTINGSBUFSZ)) return(d_MVSSET_ERRWR);
  return(d_MVSSET_OK);
}

int ReadSettingsFromFlash(void)
{
  DWORD dwCRC;
  PBYTE pbtAddr=(PBYTE)d_SETTINGS2FLASH;

  memcpy(&dwCRC,pbtAddr+d_SETTINGSBUFSZ-4,4);
  if(CalcCRC4Flash(pbtAddr,d_SETTINGSBUFSZ-4)==dwCRC)
  {
    memcpy((void *)&g_sInitData.dwIpMktu,pbtAddr,4);
    memcpy(g_IpTelNumbs,pbtAddr+4,sizeof(g_IpTelNumbs));
    CreateMACFromIP();
    return(0);
  }
  return(-1);
}

void *memfind(void *pData,int iDataLen,void *pExample,int iExampleLen)
{
  PBYTE pbtData=(PBYTE)pData;

  do
  {
    if(iExampleLen<=iDataLen)
    {
      if(!memcmp(pbtData,pExample,iExampleLen)) return(pbtData);
      pbtData++;
      iDataLen--;
    }
  }
  while(iExampleLen<=iDataLen);
  return(NULL);
}

int GetInfoElement(PCHAR pchBuf,PCHAR strInfo,int iNumbEl)
{
  int iCurEl,iLn;
  PCHAR pchStart,pchFinish;

  pchBuf[0]=0;
  pchStart=(PCHAR)strInfo;
  for(iCurEl=0;iCurEl<=iNumbEl;iCurEl++)
  {
    pchStart=strchr(pchStart,'=');
    if(!pchStart) return(0);
    pchStart++;
  }
  pchFinish=strchr(pchStart,',');
  if(!pchFinish) pchFinish=strchr(pchStart,'.');
  if(!pchFinish) return(0);
  if((pchStart+1)==pchFinish) return(0);
  iLn=pchFinish-pchStart;
  if(iLn>19) return(0);
  memcpy(pchBuf,pchStart,iLn);
  pchBuf[iLn]=0;
  return(iLn);
}

BYTE Simb2Code(char chSimb)
{
  if((chSimb>='0') && (chSimb<='9')) return (BYTE)(chSimb-'0');
  if((chSimb>='a') && (chSimb<='f')) return (BYTE)(chSimb-'a'+10);
  if((chSimb>='A') && (chSimb<='F')) return (BYTE)(chSimb-'A'+10);
  return(0xff);
}

DWORD ElementCalcCRC(PCHAR pchElement)
{
  int i,iLn=strlen(pchElement);
  DWORD dwCRC=0;

  for(i=0;i<iLn;i++) dwCRC+=~Simb2Code(pchElement[i]);
  return(dwCRC);
}

DWORD CalcCRC(PCHAR strInfo)
{
  DWORD dwCRC=0;
  static char chBuf[20];

  if(GetInfoElement(chBuf,strInfo,1)) dwCRC+=ElementCalcCRC(chBuf);
  dwCRC&=0xffff;
  if(GetInfoElement(chBuf,strInfo,2)) dwCRC+=ElementCalcCRC(chBuf);
  dwCRC&=0xffff;
  if(GetInfoElement(chBuf,strInfo,3)) dwCRC+=ElementCalcCRC(chBuf);
  dwCRC&=0xffff;
  if(GetInfoElement(chBuf,strInfo,0)) dwCRC|=ElementCalcCRC(chBuf) << 16;
  return(dwCRC);
}

DWORD STR08HEX2DW(PCHAR pchData)
{
  int i;
  BYTE btCode;
  DWORD dwRes=0;

  if((pchData[0]!='0') || (pchData[1]!='x')) return(0);
  pchData+=2;
  for(i=0;i<8;i++)
  {
    btCode=Simb2Code(pchData[i]);
    if(btCode==0xff) return(0);
    dwRes=(dwRes<<4) | btCode;
  }
  return(dwRes);
}

int MVSSettingsPacket(PCHAR pchIpData,WORD wSize0)
{
  DWORD dwCRC;
  PCHAR strMVSSet="MVS settings: IPADDR=";
  static char chBuf[20];

  if(memfind(pchIpData,wSize0,strMVSSet,strlen(strMVSSet))==(void *)pchIpData)
  {
    if(GetInfoElement(chBuf,pchIpData,4)==10)
    {
      dwCRC=STR08HEX2DW(chBuf);
      if(dwCRC==CalcCRC(pchIpData))
      {
        GetInfoElement(chBuf,pchIpData,0);
        g_sInitData.dwIpMktu=STR08HEX2DW(chBuf);
        GetInfoElement(chBuf,pchIpData,1);
        strcpy(g_IpTelNumbs[0],chBuf);
        GetInfoElement(chBuf,pchIpData,2);
        strcpy(g_IpTelNumbs[1],chBuf);
        GetInfoElement(chBuf,pchIpData,3);
        strcpy(g_IpTelNumbs[2],chBuf);
        return(StoreSettings2Flash());
      }
    }
    return(d_MVSSET_ERRFMT);
  }
  return(d_MVSSET_NOCMD);
}
