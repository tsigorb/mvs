/* The protosocket library provides an interface to the uIP stack
 * that is similar to the traditional BSD socket interface.
 * Unlike programs written for the ordinary uIP event-driven interface,
 * programs written with the protosocket library are executed in a sequential
 * fashion and does not have to be implemented as explicit state machines
 *
 * Protosockets only work with TCP connections.
 *
 * The protosocket library uses \ref pt protothreads to provide sequential control flow.
 * This makes the protosockets lightweight in terms of memory, but also means that protosockets inherits the
 * functional limitations of protothreads. Each protosocket lives only within a single function.
 * Automatic variables (stack variables) are not retained across a protosocket library function call.
 *
 * Because the protosocket library uses protothreads, local variables will not always be saved across a call
 * to a protosocket library function. It is therefore advised that local variables are used with extreme care.
 *
 * The protosocket library provides functions for sending data without having to deal
 * with retransmissions and acknowledgements, as well as functions for reading data without having to deal
 * with data being split across more than one TCP segment.
 *
 * Because each protosocket runs as a protothread, the protosocket has to be started
 * with a call to PSOCK_BEGIN() at the start of the function in which the protosocket is used.
 * Similarly, the protosocket protothread can be terminated by a call to PSOCK_EXIT().*/

#ifndef __PSOCK_H__
#define __PSOCK_H__

#include "uip_type.h"
#include "uipopt.h"
#include "pt.h"

/* Initialize a protosocket.
 * This macro initializes a protosocket and must be called before the protosocket is used.
 * The initialization also specifies the input buffer for the protosocket.
 *
 * psock (struct psock *) A pointer to the protosocket to be initialized.
 * buffer (char *) A pointer to the input buffer for the protosocket.
 * buffersize (unsigned int) The size of the input buffer. */
void psock_init(struct psock *psock, char *buffer, unsigned int buffersize);
#define PSOCK_INIT(psock, buffer, buffersize) psock_init(psock, buffer, buffersize)

/* Start the protosocket protothread in a function.
 * This macro starts the protothread associated with the protosocket and
 * must come before other protosocket calls in the function it is used.
 *
 * psock (struct psock *) A pointer to the protosocket to be started. */
#define PSOCK_BEGIN(psock) PT_BEGIN(&((psock)->pt))

/* Send data.
 * This macro sends data over a protosocket. The protosocket protothread blocks
 * until all data has been sent and is known to have been received by the remote end of the TCP connection.
 *
 * psock (struct psock *) A pointer to the protosocket over which data is to be sent.
 * data (char *) A pointer to the data that is to be sent.
 * datalen (unsigned int) The length of the data that is to be sent. */
PT_THREAD(psock_send(struct psock *psock, const char *buf, unsigned int len));
#define PSOCK_SEND(psock, data, datalen) PT_WAIT_THREAD(&((psock)->pt), psock_send(psock, data, datalen))

/* Send a null-terminated string.
 * psock Pointer to the protosocket.
 * str The string to be sent.
 *
 * This function sends a null-terminated string over the protosocket. */
#define PSOCK_SEND_STR(psock, str)      PT_WAIT_THREAD(&((psock)->pt), psock_send(psock, str, strlen(str)))
PT_THREAD(psock_generator_send(struct psock *psock, unsigned short (*f)(void *), void *arg));

/* Generate data with a function and send it
 * psock Pointer to the protosocket.
 * generator Pointer to the generator function
 * arg Argument to the generator function
 *
 * This function generates data and sends it over the protosocket.
 * This can be used to dynamically generate data for a transmission,
 * instead of generating the data in a buffer beforehand.
 * This function reduces the need for buffer memory.
 * The generator function is implemented by the application,
 * and a pointer to the function is given as an argument with the call to PSOCK_GENERATOR_SEND().
 *
 * The generator function should place the generated data directly in the g_pUipAppData buffer,
 * and return the length of the generated data.
 * The generator function is called by the protosocket layer when the data first is sent,
 * and once for every retransmission that is needed. */
#define PSOCK_GENERATOR_SEND(psock, generator, arg) \
        PT_WAIT_THREAD(&((psock)->pt), psock_generator_send(psock, generator, arg))


/* Close a protosocket.
 * This macro closes a protosocket and can only be called from within the protothread in which the protosocket lives.
 * psock (struct psock *) A pointer to the protosocket that is to be closed. */
#define PSOCK_CLOSE(psock) uip_close()

/* Read data until the buffer is full.
 * This macro will block waiting for data and read the data into the
 * input buffer specified with the call to PSOCK_INIT(). Data is read until the buffer is full.
 *
 * psock (struct psock *) A pointer to the protosocket from which data should be read. */
PT_THREAD(psock_readbuf(struct psock *psock));
#define PSOCK_READBUF(psock) PT_WAIT_THREAD(&((psock)->pt), psock_readbuf(psock))

/* Read data up to a specified character.
 * This macro will block waiting for data and read the data into the
 * input buffer specified with the call to PSOCK_INIT().
 * Data is only read until the specifieed character appears in the data stream.
 *
 * psock (struct psock *) A pointer to the protosocket from which data should be read.
 * c (char) The character at which to stop reading.*/
PT_THREAD(psock_readto(struct psock *psock, unsigned char c));
#define PSOCK_READTO(psock, c) PT_WAIT_THREAD(&((psock)->pt), psock_readto(psock, c))

/* The length of the data that was previously read.
 * This macro returns the length of the data that was previously read using PSOCK_READTO() or PSOCK_READ().
 *
 * psock (struct psock *) A pointer to the protosocket holding the data.*/
u16_t psock_datalen(struct psock *psock);
#define PSOCK_DATALEN(psock) psock_datalen(psock)


/* Exit the protosocket's protothread.
 * This macro terminates the protothread of the protosocket and should
 * almost always be used in conjunction with PSOCK_CLOSE().
 *
 * psock (struct psock *) A pointer to the protosocket.*/
#define PSOCK_EXIT(psock) PT_EXIT(&((psock)->pt))

/* Close a protosocket and exit the protosocket's protothread.
 * This macro closes a protosocket and exits the protosocket's protothread.
 *
 * psock (struct psock *) A pointer to the protosocket. */
#define PSOCK_CLOSE_EXIT(psock) \
{ PSOCK_CLOSE(psock); PSOCK_EXIT(psock); }

/* Declare the end of a protosocket's protothread.
 * This macro is used for declaring that the protosocket's protothread ends.
 * It must always be used together with a matching PSOCK_BEGIN() macro.
 *
 * psock (struct psock *) A pointer to the protosocket. */
#define PSOCK_END(psock) PT_END(&((psock)->pt))

/* Check if new data has arrived on a protosocket.
 * This macro is used in conjunction with the PSOCK_WAIT_UNTIL()
 * macro to check if data has arrived on a protosocket.
 *
 * psock (struct psock *) A pointer to the protosocket. */
char psock_newdata(struct psock *s);
#define PSOCK_NEWDATA(psock) psock_newdata(psock)

/* Wait until a condition is true.
 * This macro blocks the protothread until the specified condition is  true.
 * The macro PSOCK_NEWDATA() can be used to check if new data  arrives when the protosocket is waiting.
 * Typically, this macro is used as follows:
 PT_THREAD(thread(struct psock *s, struct timer *t))
 {
   PSOCK_BEGIN(s);
   PSOCK_WAIT_UNTIL(s, PSOCK_NEWADATA(s) || timer_expired(t));
   if(PSOCK_NEWDATA(s)) PSOCK_READTO(s, '\n');
   else handle_timed_out(s);
   PSOCK_END(s);
 }
 *
 * psock (struct psock *) A pointer to the protosocket.
 * condition The condition to wait for. */
#define PSOCK_WAIT_UNTIL(psock, condition)  PT_WAIT_UNTIL(&((psock)->pt), (condition));

#define PSOCK_WAIT_THREAD(psock, condition)  PT_WAIT_THREAD(&((psock)->pt), (condition))

#endif /* __PSOCK_H__ */
