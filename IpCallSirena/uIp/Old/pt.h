#ifndef __PT_H__
#define __PT_H__

#include "lc.h"

#define PT_WAITING      0
#define PT_EXITED       1
#define PT_ENDED        2
#define PT_YIELDED      3

/* Initialize a protothread.
 * Initializes a protothread. Initialization must be done prior to starting to execute the protothread.
 *
 *pt A pointer to the protothread control structure. */
#define PT_INIT(pt)  LC_INIT((pt)->lc)

/* Declaration of a protothread.
 * This macro is used to declare a protothread. All protothreads must be declared with this macro.
 *
 * name_args The name and arguments of the C function implementing the protothread. */
#define PT_THREAD(name_args) char name_args

/* Declare the start of a protothread inside the C function implementing the protothread.
 * This macro is used to declare the starting point of a protothread.
 * It should be placed at the start of the function in  which the protothread runs.
 * All C statements above the PT_BEGIN() invokation will be executed each time the protothread is scheduled.
 *
 * pt A pointer to the protothread control structure. */
#define PT_BEGIN(pt)    { char PT_YIELD_FLAG = 1; LC_RESUME((pt)->lc)

/* Declare the end of a protothread.
 * This macro is used for declaring that a protothread ends.
 * It must always be used together with a matching PT_BEGIN() macro.
 *
 * pt A pointer to the protothread control structure. */
#define PT_END(pt)      LC_END((pt)->lc); PT_YIELD_FLAG = 0; PT_INIT(pt); return PT_ENDED; }

/* Block and wait until condition is true.
 * This macro blocks the protothread until the specified condition is true.
 *
 * pt A pointer to the protothread control structure.
 * condition The condition. */
#define PT_WAIT_UNTIL(pt, condition)    LC_SET((pt)->lc); if(!(condition)) return PT_WAITING;

/* Block and wait while condition is true.
 * This function blocks and waits while condition is true. See PT_WAIT_UNTIL().
 *
 * pt A pointer to the protothread control structure.
 * cond The condition. */
#define PT_WAIT_WHILE(pt, cond)         PT_WAIT_UNTIL((pt), !(cond))

/* Block and wait until a child protothread completes.
 * This macro schedules a child protothread. The current protothread will block until the child protothread completes.
 * The child protothread must be manually initialized with the PT_INIT() function before this function is used.
 *
 * pt A pointer to the protothread control structure.
 * thread The child protothread with arguments */
#define PT_WAIT_THREAD(pt, thread)      PT_WAIT_WHILE((pt), PT_SCHEDULE(thread))

/* Spawn a child protothread and wait until it exits.
 * This macro spawns a child protothread and waits until it exits. The macro can only be used within a protothread.
 *
 * pt A pointer to the protothread control structure.
 * child A pointer to the child protothread's control structure.
 * thread The child protothread with arguments */
#define PT_SPAWN(pt, child, thread)     PT_INIT((child)); PT_WAIT_THREAD((pt), (thread));

/* Restart the protothread.
 * This macro will block and cause the running protothread to restart its execution at the place of the PT_BEGIN() call.
 *
 * pt A pointer to the protothread control structure. */
#define PT_RESTART(pt)      PT_INIT(pt); return PT_WAITING;

/* Exit the protothread.
 * This macro causes the protothread to exit. If the protothread was spawned by another protothread,
 * the parent protothread will become unblocked and can continue to run.
 *
 * pt A pointer to the protothread control structure. */
#define PT_EXIT(pt)     PT_INIT(pt); return PT_EXITED;

/* Schedule a protothread.
 * This function shedules a protothread. The return value of the function
 * is non-zero if the protothread is running or zero if the protothread has exited.
 *
 * f The call to the C function implementing the protothread to be scheduled */
#define PT_SCHEDULE(f) ((f) == PT_WAITING)

/* Yield from the current protothread.
 * This function will yield the protothread, thereby allowing other processing to take place in the system.
 *
 * pt A pointer to the protothread control structure. */
#define PT_YIELD(pt) \
{ PT_YIELD_FLAG=0; LC_SET((pt)->lc); if(PT_YIELD_FLAG==0) return PT_YIELDED; }

/* Yield from the protothread until a condition occurs.
 * pt   A pointer to the protothread control structure.
 * cond The condition.
 *
 * This function will yield the protothread, until the specified condition evaluates to true. */
#define PT_YIELD_UNTIL(pt, cond) \
{ PT_YIELD_FLAG = 0; LC_SET((pt)->lc); if((PT_YIELD_FLAG == 0) || !(cond)) return PT_YIELDED; }

#endif /* __PT_H__ */
