#ifndef __UIP_TYPE_H__
#define __UIP_TYPE_H__

// 8 bit datatype
// This typedef defines the 8-bit type used throughout uIP.
typedef unsigned char u8_t;

// 16 bit datatype
// This typedef defines the 16-bit type used throughout uIP.
typedef unsigned short u16_t;

typedef unsigned short lc_t;

// Statistics datatype
// This typedef defines the dataype used for keeping statistics in uIP.
typedef unsigned short uip_stats_t;

// Representation of a 48-bit Ethernet address.
struct uip_eth_addr { u8_t addr[6];};

// The Ethernet header.
struct uip_eth_hdr
{
  struct uip_eth_addr dest;
  struct uip_eth_addr src;
  u16_t type;
};


struct arp_hdr
{
  struct uip_eth_hdr ethhdr;
  u16_t hwtype;
  u16_t protocol;
  u8_t hwlen;
  u8_t protolen;
  u16_t opcode;
  struct uip_eth_addr shwaddr;
  u16_t sipaddr[2];
  struct uip_eth_addr dhwaddr;
  u16_t dipaddr[2];
};

struct ethip_hdr
{
  struct uip_eth_hdr ethhdr;
  // IP header
  u8_t vhl,tos,len[2],ipid[2],ipoffset[2],ttl,proto;
  u16_t ipchksum;
  u16_t srcipaddr[2],destipaddr[2];
};

struct arp_entry
{
  u16_t ipaddr[2];
  struct uip_eth_addr ethaddr;
  u8_t time;
};


// ��� ������ � �������� ///////////////////////////////////////
struct pt
{ lc_t lc; };

/* This structure holds the state of a uIP buffer.
   The structure has no user-visible elements, but is used through the functions provided by the library.*/
struct psock_buf
{
  u8_t *ptr;
  unsigned short left;
};

/* The representation of a protosocket.
 * The protosocket structrure is an opaque structure with no user-visible elements.*/
struct psock
{
  struct pt pt, psockpt; /* Protothreads - one that's using the psock functions,
                            and one that runs inside the psock functions. */
  const u8_t *sendptr;   // Pointer to the next data to be sent.
  u8_t *readptr;         // Pointer to the next data to be read.
  char *bufptr;          // Pointer to the buffer used for buffering incoming data.
  u16_t sendlen;         // The number of bytes left to be sent.
  u16_t readlen;         // The number of bytes left to be read.
  struct psock_buf buf;  // The structure holding the state of the input buffer.
  unsigned int bufsize;  // The size of the input buffer.
  unsigned char state;   // The state of the protosocket.
};

// ���������� ����������, ������������ � � ���������� uIP-��������� � � ����������
/* ����������� �� ������� ����� ����� ������ ��� ������ �� Ethernet.
   ����������� � ����� ����������. ������������ uIp-�������� */
extern u8_t *g_btUIPBufPtr;

/* Pointer to the application data in the packet buffer.
 * This pointer points to the application data when the application is called.
 * If the application wishes to send data, the application may use this space
 * to write the data into before calling uip_send().*/
extern void *g_pUipAppData;

/* The length of the packet in the g_btUIPBufPtr buffer.
 * The global variable g_wUipLen holds the length of the packet in the g_btUIPBufPtr buffer.
 *
 * When the network device driver calls the uIP input function,
 * g_wUipLen should be set to the length of the packet in the g_btUIPBufPtr buffer.
 * When sending packets, the device driver should use the contents of the
 * g_wUipLen variable to determine the length of the outgoing packet. */
extern u16_t g_wUipLen;


#endif
