#ifndef __MKTU_H__
#define __MKTU_H__

#include "mktu_type.h"
#include <string.h>

#define d_MVSSET_ERRFMT     -2
#define d_MVSSET_ERRWR      -1
#define d_MVSSET_EQ         0
#define d_MVSSET_OK         1
#define d_MVSSET_NOCMD      2

/////////////////////////////////////////////////////////////////////
void MktuReset(void);
void SetMktuIpMacAddrMask(InitDataTp *psInitData);

int StoreSettings2Flash(void);
int ReadSettingsFromFlash(void);

void *memfind(void *pData,int iDataLen,void *pExample,int iExampleLen);

int MVSSettingsPacket(PCHAR pchIpData,WORD wSize0);

#endif
