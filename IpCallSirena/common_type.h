#ifndef __COMMON_TYPE_H__
#define __COMMON_TYPE_H__

// 8 bit datatype
typedef char * PCHAR;
typedef unsigned char BYTE;
typedef BYTE * PBYTE;

// 16 bit datatype
typedef unsigned short WORD;
typedef WORD * PWORD;

typedef short SHORT;

// 32 bit datatype
typedef unsigned long DWORD;
typedef DWORD * PDWORD;
typedef DWORD ULONG;
typedef ULONG * PULONG;

typedef WORD BOOL;

#define TRUE        (BOOL)1
#define FALSE       (BOOL)0

#define NO_ERROR    0

typedef void (* tBufferCallback)(void *pvBuffer, unsigned long ulEvent);

#endif
