#ifndef __UIP_CONF_H__
#define __UIP_CONF_H__

#include "clock.h"
#include "common_type.h"
#include "uip/uip_type.h"

#define UIP_UDP         1
#define UIP_TCP         0

#if UIP_UDP & UIP_TCP
#error "UIP_TCP=1 and UIP_UDP=1 not used simultaneously!"
#endif

// Ping IP address assignment
// Use first incoming "ping" packet to derive host IP address
#define UIP_CONF_PINGADDRCONF       0

// UDP checksums on or off
// (not currently supported ... should be 0)
#define UIP_CONF_UDP_CHECKSUMS      0

// UDP Maximum Connections
#define UIP_CONF_UDP_CONNS          1

// Maximum number of TCP connections.
#define UIP_CONF_MAX_CONNECTIONS    1

// Maximum number of listening TCP ports.
#define UIP_CONF_MAX_LISTENPORTS    1

// Size of advertised receiver's window
//#define UIP_CONF_RECEIVE_WINDOW     400

// Size of ARP table
#define UIP_CONF_ARPTAB_SIZE        8

// uIP buffer size.
#define UIP_CONF_BUFFER_SIZE        1500

// uIP buffer is defined in the application.
#define UIP_CONF_EXTERNAL_BUFFER

// uIP statistics on or off
#define UIP_CONF_STATISTICS         0

// Logging on or off
#define UIP_CONF_LOGGING            0

// Broadcast Support
#define UIP_CONF_BROADCAST          0

// Link-Level Header length
#define UIP_CONF_LLH_LEN            14

// CPU byte order.
#define UIP_CONF_BYTE_ORDER         UIP_LITLE_ENDIAN

// UIP_APPCALL: the name of the application function. This function
// must return void and take no arguments (i.e., C type "void appfunc(void)").
void mktu_appcall(void);

#define UIP_APPCALL     mktu_appcall

#endif // __UIP_CONF_H_
